import os
import math

import numpy
from scipy.optimize import fsolve

import lal
import lalsimulation

#
# disk mass formulas, Foucart 2012
# https://arxiv.org/abs/1207.6304
#
# NSBH ejecta formula, Kawaguchi et. al. 2016
# http://iopscience.iop.org/article/10.3847/0004-637X/825/1/52/pdf
#
# BNS ejecta formula, T. Dietrich and M. Ujevic 2016
# https://arxiv.org/pdf/1612.03665.pdf
#

EOS_PATH = "/Users/chrispankow/work/CIERA/LSST+LIGO/notebooks/eos_tables/"

def create_family(eos_name):
    """
    Generate an EOS family from its canonical name. Requires the path EOS_PATH in this module to point towards a directory holding EOS tables.

    Adapted from code by Monica Rizzo.
    """

    path = os.path.join(EOS_PATH, "%s.dat" % eos_name.lower())
    if os.path.exists(path):
        bdens, press, edens = numpy.loadtxt(path, unpack=True)
        press *= 7.42591549e-25
        edens *= 7.42591549e-25
        eos_name = os.path.basename(path)
        eos_name = os.path.splitext(eos_name)[0].upper()

        if not numpy.all(numpy.diff(press) > 0):
            keep_idx = numpy.where(numpy.diff(press) > 0)[0] + 1
            keep_idx = numpy.concatenate(([0], keep_idx))
            press = press[keep_idx]
            edens = edens[keep_idx]
        assert numpy.all(numpy.diff(press) > 0)

        if not numpy.all(numpy.diff(edens) > 0):
            keep_idx = numpy.where(numpy.diff(edens) > 0)[0] + 1
            keep_idx = numpy.concatenate(([0], keep_idx))
            press = press[keep_idx]
            edens = edens[keep_idx]
        assert numpy.all(numpy.diff(edens) > 0)

        rnd = numpy.random.randint(0, 2**32)
        fname = "./." + eos_name + "_%d.dat" % rnd
        if os.path.exists(fname):
            os.unlink(fname)
        numpy.savetxt(fname, numpy.transpose((press, edens)), delimiter='\t')
        eos = lalsimulation.SimNeutronStarEOSFromFile(str(fname))
        fam = lalsimulation.CreateSimNeutronStarFamily(eos)
        os.unlink(fname)
    else:
        #print "Using EoS from lalsimulation %s" % eos_name
        eos = lalsimulation.SimNeutronStarEOSByName(str(eos_name.upper()))
        fam = lalsimulation.CreateSimNeutronStarFamily(eos)

    return eos, fam

def multi_root(function, root_num, upper_bound, lower_bound):
    guess = upper_bound
    step = 0.05
    answer_array = numpy.array([])

    while len(answer_array) < root_num:
        answer_tmp = fsolve(function, guess)
        value_existing=False
        if len(answer_array) > 0:
            for ans in answer_array:
                value_existing |= numpy.isclose(abs(answer_tmp), abs(ans), rtol=1e-06)
            if not value_existing:
                answer_array = numpy.append(answer_array, abs(answer_tmp))
        else:
            answer_array = numpy.append(answer_array, answer_tmp)

        guess -= step
        if guess < lower_bound:
            guess = upper_bound
            step /= 2.

    return answer_array


def r_isco(chi_bh):
    # eqn. 3
    z_1 = 1 + (1 - chi_bh**2)**(1./3) * ((1 + chi_bh)**(1./3) + (1 - chi_bh)**(1./3))
    s_chi = 1 if chi_bh == 0 else (chi_bh / numpy.abs(chi_bh))
    z_2 = numpy.sqrt(3 * chi_bh**2 + z_1**2)
    return 3 + z_2 - s_chi * numpy.sqrt((3 - z_1) * (3 + z_1 + 2 * z_2))

def solve_xi_tidal(m_bh, m_ns, chi_bh, r_ns):
    """
    Solve for the \\xi_{tidal} parameter (eqn 7) implicitly.
    """

    kappa = m_bh / r_ns * lal.MSUN_SI * lal.G_SI / lal.C_SI**2

    # Solve equation where beta^2 == xi
    def _implicit(beta):
        _t1 = m_ns * beta**6 * (beta**4 - 3. * beta**2 * kappa + 2. * numpy.sqrt(beta**2 * kappa**3) * chi_bh) - 3. * m_bh * (beta**4 - 2. * beta**2 * kappa + kappa**2 * chi_bh**2)
        return _t1

    roots = multi_root(_implicit, 3, 4., 0.)

    return max(roots)**2.

# eqn 14 and 15
_alpha_tilde, _beta_tilde = 0.296, 0.171

# eqn 8.
# FIXME: Needs more testing
def frac_disk_mass(m_bh=10., m_ns=1.4, chi_bh=0.1, r_ns=14e3):
    """
    Fraction of the neutron star mass which remains after disruption. Masses are in solar masses, spin parameter is dimensionless, and the neutron star radius is in meters.
    """
    # NS compactness
    c_ns = m_ns / r_ns * lal.G_SI / lal.C_SI**2 * lal.MSUN_SI

    # ratio of tidal separation vs. NS radius
    xi_tidal = solve_xi_tidal(m_bh, m_ns, chi_bh, r_ns)

    # ISCO radius
    r_isco_scaled = r_isco(chi_bh) * m_bh * lal.G_SI / lal.C_SI**2 * lal.MSUN_SI

    # fit coefficients
    alpha_tilde, beta_tilde = _alpha_tilde, _beta_tilde

    return alpha_tilde * xi_tidal * (1 - 2 * c_ns) - beta_tilde * r_isco_scaled / r_ns

_a1, _a2, _a3, _a4, _n1, _n2 = 4.464e-2, 2.269e-3, 2.431, -0.4159, 0.2497, 1.352

def nsbh_ejecta_mass(m_bh=10., m_ns=1.4, chi_bh=0.1, i_tilt=0., r_ns=14e3, mb_ns=0.):
    """
    Neutron star baryonic mass ejected (M_sun). Masses in solar masses, spin parameter is dimensionless, i_tilt is the angle between the spin and the orbital angular momentum (in radian), neutron star radius in meters, and baryon mass, if provided, is in solar masses.
    """

    # ns compactness
    c1 = m_ns / r_ns * lal.G_SI / lal.C_SI**2 * lal.MSUN_SI

    # Values from Coughlin, et al. 2017
    if mb_ns==0.:
       mb_ns = (1 + 0.8858 * c1**1.2082) * m_ns

    # NS compactness
    c_ns = m_ns / r_ns * lal.G_SI / lal.C_SI**2 * lal.MSUN_SI

    # Chi_eff from paper
    chi_eff = chi_bh * numpy.cos(i_tilt)

    #coefficients
    a1, a2, a3, a4, n1, n2 = _a1, _a2, _a3, _a4, _n1, _n2

    r_isco_tilde=r_isco(chi_eff)

    # mass ratio
    q = m_bh / m_ns

    return max(a1 * q**n1 * (1. - 2.*c_ns) * c_ns**(-1) - a2 * q**n2 * r_isco_tilde + a3 * (1. - m_ns / mb_ns) + a4, 0.) * mb_ns

_a, _b, _c, _d, _n= -1.35695, 6.11252, -49.43355, 16.1144, -2.5484

def bns_ejecta_mass(m_ns1=1.4, m_ns2=1.75, r_ns1=10e3, r_ns2=10e3, mb_ns1=0., mb_ns2=0.):
    """
    Neutron star mass ejected (M_sun). Masses in solar masses, neutron star radius in meters, and baryon mass, if provided, is in solar masses.
    """

    # ns compactness
    c1 = m_ns1 / r_ns1 * lal.G_SI / lal.C_SI**2 * lal.MSUN_SI
    c2 = m_ns2 / r_ns2 * lal.G_SI / lal.C_SI**2 * lal.MSUN_SI

    # Values from Coughlin, et al. 2017
    if mb_ns1==0.:
       #mb_ns1 = m_ns1 + 0.2 * m_ns1
       mb_ns1 = (1 + 0.8858 * c1**1.2082) * m_ns1

    if mb_ns2==0.:
       #mb_ns2 = m_ns2 + 0.2 * m_ns2
       mb_ns2 = (1 + 0.8858 * c2**1.2082) * m_ns2

    a, b, c, d, n = _a, _b, _c, _d, _n

    return ((a* (m_ns2 / m_ns1)**(1 / 3.) * (1 - 2 * c1) / c1 + b * (m_ns2 / m_ns1)**n + c * (1.- m_ns1 / mb_ns1)) * mb_ns1  \
           + (a * (m_ns1 / m_ns2)**(1/3.) * (1 -2 * c2) / c2 + b * (m_ns1 / m_ns2)**n + c * (1. - m_ns2 / mb_ns2)) * mb_ns2 + d) * 10**-3

#
# Testing
#
if __name__ == "__main__":
    #print r_isco(0.0), r_isco(-0.5), r_isco(0.9)
    #print solve_xi_tidal(10., 1.4, 0.5, 13.5e3)
    #print frac_disk_mass(m_bh=1.4 * 4, m_ns=1.4, chi_bh=0.8, r_ns=13.5e3)

    print bns_ejecta_mass()
    print nsbh_ejecta_mass()


    from matplotlib import pyplot
    from scipy.stats import uniform
    samples = 1000
    m_bh = numpy.random.uniform(3., 10., samples)
    m_ns = numpy.random.uniform(1., 2., samples)
    chi_bh = numpy.random.uniform(0., 1., samples)
    #r_ns = numpy.random.uniform(10.e3, 16.e3, samples)
    r_ns = numpy.ones(samples) * 13.5e3
    dist = numpy.random.power(3, samples) * 1000 # Mpc

    dm = []
    for mb, mn, cb, rn in numpy.transpose(numpy.asarray((m_bh, m_ns, chi_bh, r_ns))):
        dm.append(frac_disk_mass(mb, mn, cb, rn))
    dm = numpy.asarray(dm)

    pyplot.figure(figsize=(20, 5))
    mask = dm > 0
    pyplot.scatter(m_bh, m_ns, c='k', edgecolor='none')
    pyplot.scatter(m_bh[mask], m_ns[mask], c=dm[mask], vmin=0., edgecolor='none')
    pyplot.colorbar()
    pyplot.xlabel("BH mass")
    pyplot.ylabel("NS mass")
    pyplot.savefig("em_bright_test.png")
