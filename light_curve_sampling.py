
import numpy
from scipy.stats import chi2, norm, uniform
from scipy.optimize import brentq

import lal

# internight revisit prob (days)
def revisit(dt):
    pdf = norm.pdf(dt, 1, 0.1)
    pdf += norm.pdf(dt, 2, 0.1)
    pdf += norm.pdf(dt, 3, 0.1)
    return pdf / 3

def sample_revisit():
    x = uniform.rvs(0, 3)
    dt = numpy.linspace(0, 4, 1000)
    cdf = norm.cdf(dt, 1, 0.1)
    cdf += norm.cdf(dt, 2, 0.1)
    cdf += norm.cdf(dt, 3, 0.1)
    return dt[numpy.searchsorted(cdf, x)]

def sample_revisit_rs():
    while True:
        x = uniform.rvs(0, 4)
        if uniform.rvs(0, 1.5) < revisit(x):
            break
    return x

def generate_obs(duration=10, inter_night_offset=3):
    # FIXME: It's possible to observe it on the first night, but that isn't
    # taken into account here.
    #t0 = [uniform.rvs(0, inter_night_offset)]
    t0 = [sample_revisit()]
    while True:
        tn = sample_revisit()
        if tn + t0[-1] > duration:
            break
        t0.append(t0[-1] + tn)

    return numpy.asarray(t0)
