#!/usr/bin/env python
import itertools
import numpy
from matplotlib import pyplot
from scipy.stats import lognorm, poisson
from scipy.integrate import fixed_quad, trapz, cumtrapz

#
# Adapted from: https://arxiv.org/pdf/1003.2480.pdf
#
# numbers in above publication are Mpc^-3 Myr^-1, we convert to Mpc^-3 yr^-1
#BNS_RATES_LOW = 1e-2 * 1e-6
#BNS_RATES_LIKELY = 1. * 1e-6
#BNS_RATES_HIGH = 1e1 * 1e-6

NSBH_RATES_LOW = 6e-4 * 1e-6
NSBH_RATES_LIKELY = 3e-2 * 1e-6
NSBH_RATES_HIGH = 1. * 1e-6

# From GW170817 discovery
# https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.119.161101
# numbers in above publication are Gpc^-3 yr^-1, we convert to Mpc^-3 yr^-1
BNS_RATES_LIKELY = 1540 * 1e-9
BNS_RATES_LOW = BNS_RATES_LIKELY - 1220 * 1e-9
BNS_RATES_HIGH = BNS_RATES_LIKELY + 3200 * 1e-9

# From GW170104
# https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.118.221101
# numbers in above publication are Gpc^-3 yr^-1, we convert to Mpc^-3 yr^-1
BBH_RATES_LOW = 12 * 1e-9
BBH_RATES_HIGH = 213 * 1e-9
# NOTE: "likely" isn't accurate since we merged two distributions
BBH_RATES_LIKELY = 103 * 1e-9

#
# Utilities for generating rate distributions
#

def solve_lognorm_params(low, high):
    """
    Obtain parameters for the scipy lognorm distribution by fitting mu and sigm to the low and high numbers for a given category of rates. Thanks to Will Farr for some guidance in this matter.
    """
    mu = 0.5 * (numpy.log(high) + numpy.log(low))
    sigma = 0.25 * (numpy.log(high) - numpy.log(low))
    return mu, sigma

def generate_rate_prior(low, high):
    mu, sigma = solve_lognorm_params(low, high)
    return lognorm(sigma, scale=numpy.exp(mu))

def n_given_rate(r, n, vt):
    _lambda = r * vt
    return poisson(_lambda).pmf(n)

#
# Rate integration
#

def integrate_rates(r, n, rlow, rhigh, vt):

    # This is our rate distribution
    rate_distr = generate_rate_prior(rlow, rhigh)

    # We're integrating the rate prior against p(n|R), likely a poissionian
    # distribution with the Poisson mean given by RVT
    def integrand(r, n, vt):
        return rate_distr.pdf(r) * n_given_rate(r, n, vt=vt)

    intg = [integrand(r, _n, vt) for _n in n]

    p_n = numpy.array([trapz(intg, r)]).flatten()

    return p_n, numpy.asarray(intg)

#
# Event based detection probabilities
#

def _prb_n_detect(prb):
    nprb = 1. - prb
    idx = set(range(len(prb)))
    n_prb = numpy.zeros(len(prb)+1)
    n_prb[0] = numpy.prod(nprb)
    n_prb[-1] = numpy.prod(prb)
    for i in range(1, len(prb)):
        # Detection probs
        for comb in itertools.combinations(idx, i):
            det = numpy.asarray(comb)
            # Complement is misses
            miss = numpy.asarray(list(idx - set(det)))
            n_prb[i] += numpy.prod(prb[det]) * numpy.prod(nprb[miss])

    return n_prb

def prb_n_detect(prb):
    """
    The straightforward way of calculating the P(n_obs|{p}_i) is computation unfeasinble for large i \\in N. This is the "discrete Fourier transform" method, explained in https://en.wikipedia.org/wiki/Poisson_binomial_distribution . It is a refinement of a recursive method which is much faster, but numerically unstable.
    """
    n = numpy.arange(0, len(prb)+1)
    c_n = numpy.exp(2j * numpy.pi / len(n))
    c_l = c_n**(n + 1)
    n_prb = 1 + numpy.outer(prb, c_l - 1)
    c_k = numpy.asarray([c_l**-k for k in n])
    n_prb = c_k * n_prb.prod(axis=0)
    return (1. / len(n)) * numpy.real_if_close(n_prb.sum(axis=1))


if __name__ == "__main__":
    dist = generate_rate_prior(BNS_RATES_LOW, BNS_RATES_HIGH)
    print "BNS rates, cdf at low: %1.3f, high: %1.3f" % \
        (dist.cdf(BNS_RATES_LOW), dist.cdf(BNS_RATES_HIGH))

    dist = generate_rate_prior(NSBH_RATES_LOW, NSBH_RATES_HIGH)
    print "NSBH rates, cdf at low: %1.3f, high: %1.3f" % \
        (dist.cdf(NSBH_RATES_LOW), dist.cdf(NSBH_RATES_HIGH))

    x = numpy.logspace(-5 - 6 , 3 - 6 , 1000)
    pyplot.subplot(2, 1, 1)
    pyplot.plot(x, dist.pdf(x))
    rvs = dist.rvs(100000)
    bins = numpy.logspace(-5 - 6 , 3 - 6 , 100)
    pyplot.hist(rvs, bins=bins, histtype='stepfilled', normed=True)
    pyplot.semilogx()
    pyplot.subplot(2, 1, 2)
    pyplot.plot(x, dist.cdf(x))
    pyplot.semilogx()
    pyplot.legend()
    pyplot.savefig("rates_test.png")
