from __future__ import division
import matplotlib
matplotlib.use("agg")

import matplotlib.pyplot as plt
import numpy as np
import glob
from argparse import ArgumentParser
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MaxNLocator
import glob

# Input file name
_network = ("H1", "L1", "V1")
argp = ArgumentParser()
argp.add_argument("-i", "--filename", default="temp", help="Specify filename for text file with localization statistics")
argp.add_argument("-o", "--output", default="temp", help="Specify base name for plots.")
argp.add_argument("-t", "--title", default="Localization Statistics", help="Provide string title for plots")
argp.add_argument("-s", "--snr-threshold", default=0, help="Provide numerical value forSNR Threshold to make scatterplots")
args = argp.parse_args()

# Read input sky areas and SNRs for corresponding events
loc_data = []
snrs = []
for filename in glob.glob(args.filename):
    if filename.split('/')[-2] not in ["outputfiles"]: #["event.501", "event.502", "event.926"]:
        loc_file = filename + "localize/areas.txt"
        snr_file = filename + "event_snr.txt"
        loc_data.append(np.loadtxt(loc_file, delimiter=','))
        snrs.append(np.loadtxt(snr_file, delimiter=','))


loc_data = np.asarray(loc_data)
snrs = np.asarray(snrs)

sky_area90 = loc_data[:,-1]


# Calculate 50 and 90% credible regions for sky areas
err_med = np.percentile(sky_area90, 50)
err_90 = np.percentile(sky_area90, 90)

# Read in the SNRs. This should be sorted by run number by default 
#snrs = np.loadtxt("event_snr.txt", delimiter=",")

# For each event, check how many IFOs have SNRs > 5.5
snr_cutoffs_marker = []
for index, event in enumerate(snrs):
    # Sort IFO SNRs by value
    ifo_snrs = np.sort(event[2:])
    # If all IFOs have an SNR above 5.5
    if ifo_snrs[0] > 5.5:
        snr_cutoffs_marker.append('o')
    else:
        snr_cutoffs_marker.append('*')

# Make a histogram of all the sky areas
plt.figure(1)
plt.hist(sky_area90, color='0.75', histtype='stepfilled', bins=np.logspace(-1, 5, 30))
plt.axvline(x=err_med, color='k', linewidth=2, label=('Median'))
plt.axvline(x=err_90, color='k', linewidth=2, ls='-.', label=('90%'))
plt.xlabel('Sky Area 90% Cred. Region (squared degrees)')
plt.ylabel('Count')
plt.xlim(1e-1, 1e5)
plt.xscale('log')
plt.legend(fontsize=10)
plt.title(args.title)
plt.savefig("%s_skyarea.png" % args.output)



# Make a histogram with scatterplot below
plt.figure(2)
gs = gridspec.GridSpec(3, 3)
gs.update(hspace=0.0, wspace=0.0)
ax1 = plt.subplot(gs[1:, :])

labeled_highsnr = False
labeled_lowsnr = False
# FIXME: there must be a better way to only label the first occurence of a certain point
for i in np.arange(len(sky_area90)):
    if not labeled_lowsnr and snr_cutoffs_marker[i] == '*':
        ax1.scatter(sky_area90[i], snrs[:,1][:len(sky_area90)][i], c='m', marker=snr_cutoffs_marker[i], edgecolors='None', label='Not All IFO SNR < 5.5')
        labeled_lowsnr = True
    if not labeled_highsnr and snr_cutoffs_marker[i] == 'o':
        ax1.scatter(sky_area90[i], snrs[:,1][:len(sky_area90)][i], c='m', marker=snr_cutoffs_marker[i], edgecolors='None', label='All IFO SNR > 5.5')
        labeled_highsnr = True
    else:
        ax1.scatter(sky_area90[i], snrs[:,1][:len(sky_area90)][i], c='m', marker=snr_cutoffs_marker[i], edgecolors='None')
if args.snr_threshold > 0:
    ax1.axhline(y=args.snr_threshold, color='k', linewidth=2, ls='-.', label='SNR Threshold')
ax1.set_xlabel('Sky Area 90% Cred. Region (squared degrees)')
ax1.set_ylabel('Network SNR')
ax1.set_xlim(1e-1, 1e5)
ax1.set_ylim(0, 60)
ax1.set_xscale('log')
ax1.legend(fontsize=10)

ax2 = plt.subplot(gs[0:1, :], sharex=ax1)
ax2.hist(sky_area90, color='0.75', histtype='stepfilled', bins=np.logspace(-1, 5, 30))
if args.snr_threshold > 0:
    ax2.hist(sky_area90[np.where(snrs[:,1] > float(args.snr_threshold))],
    color='c', alpha=0.5, histtype='stepfilled', bins=np.logspace(-1, 5, 30), label='Above SNR Threshold')
ax2.tick_params(axis='x', top='on', bottom='on', labelbottom='off', labeltop='off')
nbins = len(ax1.get_xticklabels())
ax2.yaxis.set_major_locator(MaxNLocator(nbins=nbins, prune='lower', integer=True))
ax2.get_yaxis().set_ticks([])
ax2.axvline(x=err_med, color='k', linewidth=2, label=('Median'))
ax2.axvline(x=err_90, color='k', linewidth=2, ls='-.', label=('90%'))
#ax2.set_xlim(1e-2, 1e5)
#ax2.set_xscale('log')
ax2.legend(fontsize=10)

print err_med, err_90

plt.suptitle(args.title)

plt.savefig('%s_snr_skyarea.png' % args.output)






# Make a stacked histogram with scatterplot below
plt.figure(3)
gs = gridspec.GridSpec(3, 3)
gs.update(hspace=0.0, wspace=0.0)
ax1 = plt.subplot(gs[1:, :])

labeled_highsnr = False
labeled_lowsnr = False
# FIXME: there must be a better way to only label the first occurence of a certain point
for i in np.arange(len(sky_area90)):
    if not labeled_lowsnr and snr_cutoffs_marker[i] == '*':
        ax1.scatter(sky_area90[i], snrs[:,1][:len(sky_area90)][i], c='m', marker=snr_cutoffs_marker[i], edgecolors='None', label='Not All IFO SNR < 5.5')
        labeled_lowsnr = True
    if not labeled_highsnr and snr_cutoffs_marker[i] == 'o':
        ax1.scatter(sky_area90[i], snrs[:,1][:len(sky_area90)][i], c='m', marker=snr_cutoffs_marker[i], edgecolors='None', label='All IFO SNR > 5.5')
        labeled_highsnr = True
    else:
        ax1.scatter(sky_area90[i], snrs[:,1][:len(sky_area90)][i], c='m', marker=snr_cutoffs_marker[i], edgecolors='None')
ax1.set_xlabel('Sky Area 90% Cred. Region (squared degrees)')
ax1.set_ylabel('Network SNR')
ax1.set_xlim(1e-1, 1e5)
ax1.set_ylim(0, 60)
ax1.set_xscale('log')
ax1.legend(fontsize=10)


ax2 = plt.subplot(gs[0:1, :], sharex=ax1)

# Find points where all IFO's through just 2 IFO's meet SNR requirement
loc_for_hist = []
sorted_snrs = np.sort(snrs[:,2:])
hist_labels = []
num_hists = len(sorted_snrs[0]) - 1
for i in np.arange(num_hists):
    # All IFO's above 5.5
    if i == 0:
        loc_for_hist.append(sky_area90[np.where(sorted_snrs[:,0] > 5.5)])
        hist_labels.append('All IFO SNR > 5.5')
    else:
        loc_for_hist.append(sky_area90[np.where(np.logical_and(
            (sorted_snrs[:,(i-1)] < 5.5), (sorted_snrs[:,i] > 5.5)))]) 
        hist_labels.append('%i IFO SNR < 5.5' % i)

# Invert order of histograms and labels
loc_for_hist = loc_for_hist[::-1]
hist_labels = hist_labels[::-1]

# Colors for stacked hist
#colors = [(x/(2*num_hists), x/(num_hists), 0.85) for x in np.arange(num_hists)]
colors = ['0.75', 'cyan', 'magenta', 'blue'][(4-num_hists):]

ax2.hist(loc_for_hist, color=colors, label=hist_labels, histtype='barstacked', stacked=True, bins=np.logspace(-1, 5, 30))
ax2.tick_params(axis='x', top='on', bottom='on', labelbottom='off', labeltop='off')
nbins = len(ax1.get_xticklabels())
ax2.yaxis.set_major_locator(MaxNLocator(nbins=nbins, prune='lower', integer=True))
ax2.get_yaxis().set_ticks([])
ax2.axvline(x=err_med, color='k', linewidth=2, label=('Median'))
ax2.axvline(x=err_90, color='k', linewidth=2, ls='-.', label=('90%'))
#ax2.set_xlim(1e-2, 1e5)
#ax2.set_xscale('log')
ax2.legend(fontsize=10)

print err_med, err_90

plt.suptitle(args.title)

plt.savefig('%s_stacked_hist.png' % args.output)


