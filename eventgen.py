from __future__ import print_function
from collections import OrderedDict
from functools import partial

import numpy
from numpy.random import uniform, power, normal

from scipy.integrate import cumtrapz
from scipy.interpolate import interp1d

import h5py

import lal
import lalsimulation
try:
    from lalinference.rapid_pe.lalsimutils import Mceta
except ImportError:
    def Mceta(m1, m2):
        """Compute chirp mass and symmetric mass ratio from component masses"""
        Mc = (m1*m2)**(3./5.)*(m1+m2)**(-1./5.)
        eta = m1*m2/(m1+m2)/(m1+m2)
        return Mc, eta

from lalinference.io.events import Event as GenericEvent
from lalinference.io.events import SingleEvent

#
# Distributions not in scipy
#
def negative_power(a, b, idx):
    if idx < 0:
        assert b > a > 0
    else:
        assert b > a

    if idx == -1:
        norm = b / a
        rvs = numpy.random.uniform(0, 1)
        return norm**rvs * a

    norm = b**(idx+1) - a**(idx+1)
    rvs = numpy.random.uniform(0, 1)
    # cdf = (rvs**(idx+1) - a**(idx+1)) / norm
    return (norm * rvs + a**(idx+1))**(1.0/(idx+1))

#
# Attribute generators
#

# Source orientation generators
_extr = ("right_ascension", "declination", "inclination", "polarization", "coa_phase")
def random_orientation():
    ra = uniform(0, 2 * numpy.pi)
    dec = numpy.arcsin(uniform(-1, 1))
    incl = numpy.arccos(uniform(-1, 1))
    psi, phi = uniform(0, numpy.pi), uniform(0, 2 * numpy.pi)
    return ra, dec, incl, psi, phi

_orien = ("inclination", "polarization", "coa_phase")
def optimally_oriented():
    # Inclination, coa_phase (ignored), polarization (ignored)
    return 0., 0., 0.

def theta(det, time, ra, dec, pol, incl):
    """
    Amplitude orientation scaling factor, see Finn and Chernoff, 1996, eqn. 3.31. Note that this returns theta, not theta^2. So the range of this variable is 0 <= theta <= 4.

    >>> round(theta("H1", 1e9, 0., 0., 0., 0.,), 5)
    1.17058
    """
    rp = lal.cached_detector_by_prefix[det].response
    t = lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(time))
    fp, fx = lal.ComputeDetAMResponse(rp, ra, dec, pol, t)
    cos_inc2 = numpy.cos(incl)**2
    return 2 * numpy.sqrt(fp**2 * (1 + cos_inc2)**2 + 4 * fx**2 * cos_inc2)

# Distance generators
def uniform_volume(d_max=1000):
    """
    Uniform in Euclidean volume. E. g. p(r) \propto r^2
    """
    # Note power ~ (idx - 1)
    return (power(3) * d_max,)

# Redshift generators

#
# This is to hack around the painfully slow cosmology calculator from astropy
#

def init_cosmology(min_log10_z=-2, max_log10_z=1):
    """
    Damn astropy, you slow.
    """
    from astropy.cosmology import Planck15
    _com_z_cache = numpy.logspace(min_log10_z, max_log10_z, 1000)
    _com_cache = Planck15.comoving_volume(_com_z_cache).value
    _com_d_cache = Planck15.comoving_distance(_com_z_cache).value
    com_v = lambda z: _com_cache[numpy.searchsorted(_com_z_cache, z)]
    com_d = lambda z: _com_cache[numpy.searchsorted(_com_d_cache, z)]
    return com_v, com_d

# NOTE: One will have to recall this idiom to reset the internal interpolators
_com, _com_dist = init_cosmology()

#
# Redshift distributions derived from SFRs
#

def madau_dickinson_sfr(z):
    """
    Redshift distributon over the SFR from Madau and Dickinson (2014), eqn 15.
    """
    coef, num_exp, den_coef, den_exp = 0.015, 2.7, 1/2.9, 5.6
    return coef * (1 + z)**num_exp / (1 + (den_coef * (1 + z))**den_exp)

# Form CDF over complete range of "validity"
_md_zvals = numpy.linspace(0, 10, 1000)
# CDF
_md_cdf = cumtrapz(madau_dickinson_sfr(_md_zvals), _md_zvals)
# Interpolated inverse CDF
_md_invcdf = interp1d(_md_cdf, _md_zvals[:-1])
# Interpolated CDF
_md_cdf = interp1d(_md_zvals[:-1], _md_cdf)

def madau_dickinson_redshift(z_max, z_min=0):
    """
    Obtain redshift distributed as the SFR inferred from Madau and Dickinson (2014), eqn 15.
    """
    assert z_min < z_max
    if z_max >= _md_zvals[-1]:
        print("Invalid maximum redshift (z={0:f}), MD SFR only valid up to z ~ 8-10.".format(z_max), file=sys.stderr)
    n1, n2 = _md_cdf(z_max), _md_cdf(z_min)
    rv = uniform(n1, n2)
    return (_md_invcdf(rv),)

def pl_redshift(z_max, z_min=0, index=3):
    """
    Obtain redshift distributed as (1+z)^index
    """
    assert z_min < z_max
    if index < 0:
        assert z_min > 0

    # Shifted negative power distr.
    a, b, idx = z_min, z_max, index
    norm = (1 + b)**(idx+1) - (1 + a)**(idx+1)
    rvs = numpy.random.uniform(0, 1)
    return ((norm * rvs + (1 + a)**(idx+1))**(1.0/(idx+1)) - 1,)

    # Despite its name, it can handle positive powers too.
    #return (negative_power(z_min, z_max, index),)

def uniform_comoving_redshift(z_max, z_min=0):
    """
    Obtain redshift distributed as uniform in comoving volume.
    """
    assert z_min < z_max
    from astropy.cosmology import Planck15
    #_fmax = Planck15.comoving_volume(z_max).value
    _fmin = _com(z_min) if z_min > 0 else 0
    _fmax = _com(z_max)
    assert _fmin < _fmax
    while True:
        f, z = uniform(_fmin, _fmax), uniform(z_min, z_max)
        #if f < Planck15.comoving_volume(z).value:
        if f < _com(z):
            return (z,)

_REDSHIFT_GENERATORS = {
    "uniform_comoving": uniform_comoving_redshift,
    "madau_dickinson": madau_dickinson_redshift,
    "power_law": pl_redshift
}

def get_mass_and_redshift_generator(net, snr_thresh, \
                                    redshift_gen=uniform_comoving_redshift, \
                                    ref_ifo=None, min_mass=3, max_mass=50):
    """
    Return a function which uses a pre-generated look up table to associate a maximum redshift with a total mass over the range specified by min_mass and max_mass. This function will call `redshift_gen` to generate a redshift which assumes a given distribution normalized properly over z_max (first argument) and z_min (optional second argument). The masses are generated according to a different (currently hardcoded) function.
    """

    #
    # Generate look up table
    #
    mass_grid = numpy.linspace(min_mass, max_mass, 100)
    if ref_ifo is not None:
        lookup_table = [net.redshift_horizon(mass1=m, mass2=m, \
                                        snr_threshold=snr_thresh)[ref_ifo] \
                                                for m in mass_grid]
    else:
        lookup_table = [max(net.redshift_horizon(mass1=m, mass2=m, \
                                        snr_threshold=snr_thresh).values()) \
                                                for m in mass_grid]
    lookup_table = numpy.asarray(lookup_table)

    # Grid is in total mass
    mass_grid += mass_grid

    def astro_bbh_comp_mass_and_z(min_bh_mass, max_bh_mass, max_mtotal=100, \
                                    min_q=0.1, max_q=1.0, z_min=0, z_max=None):
        """
        Draw masses according to `astro_bbh_comp_mass`, and then, based on a predefined lookup table, determine a z_max with which to appropriately draw a redshift from. If specified, z_min can also be used.
        """

        cut = lookup_table > z_min
        # Make sure we have something to work with
        assert numpy.any(cut)

        # Check if z_min cuts out part of our mass parameter space
        if numpy.any(~cut):
            #print "z min will eliminate part of lookup table"

            # Temporary for now, will need better way to handle this
            #mt_min, mt_max = min(lookup_table[cut]), max(lookup_table[cut])

            mtot_idx = None
            while mtot_idx is None:
                #m1, m2 = power_law_flat(mt_min * min_q, max_bh_mass, mt_max, \
                                                #min_q, max_q)
                m1, m2 = power_law_flat(min_bh_mass, max_bh_mass, max_mtotal, \
                                                min_q, max_q)
                mtot = m1 + m2
                mtot_idx = numpy.searchsorted(mass_grid, mtot, side='right')
                # FIXME, this will become more difficult as the valid slices in 
                # total mass become smaller in extent
                mtot_idx = mtot_idx if cut[mtot_idx] else None

        else:
            # Draw new masses
            m1, m2 = power_law_flat(min_bh_mass, max_bh_mass, max_mtotal, \
                                            min_q, max_q)

        # Draw redshift based on masses
        mtot = m1 + m2
        mtot_idx = numpy.searchsorted(mass_grid, mtot, side='right')
        new_zmax = lookup_table[mtot_idx]
        new_zmax = min(new_zmax, z_max) if z_max is not None else new_zmax

        #assert new_zmax > aligo.redshift_horizon(mass1=m1, mass2=m2, \
                                                #snr_threshold=snr_thresh)["H1"]
        return m1, m2, redshift_gen(new_zmax, z_min)[0]

    return astro_bbh_comp_mass_and_z

def uniform_comoving(z_max):
    """
    Uniform in comoving volume based on Planck15 cosmology.
    """
    from astropy.cosmology import Planck15
    return Planck15.luminosity_distance(uniform_comoving_redshift(z_max)).value

# Useful to post-convert redshift into distance
def uniform_comoving_dist(event):
    from astropy.cosmology import Planck15
    return (Planck15.comoving_distance(event.z).value,)

def uniform_luminosity_dist(event):
    from astropy.cosmology import Planck15
    return (Planck15.luminosity_distance(event.z).value,)

# Mass generators
def delta_fcn_comp_mass(mass1=10., mass2=10.):
    """
    Generate mass pairs with specific values.
    """
    return mass1, mass2

def uniform_comp_mass(min_primary=1.0, max_primary=100.0, min_secondary=1.0, max_secondary=100.0):
    return uniform(min_primary, max_primary), \
           uniform(min_secondary, max_secondary)

def normal_comp_mass(mean_primary=1.4, std_primary=0.1, mean_secondary=1.4, std_secondary=0.1):
    return normal(mean_primary, std_primary), \
           normal(mean_secondary, std_secondary)

def astro_comp_mass(min_primary=5.0, max_primary=100.0, mean_secondary=1.4, std_secondary=0.1, power_idx=-2.3):
    intrv = max_primary - min_primary
    return negative_power(min_primary, max_primary, power_idx), \
            normal(mean_secondary, std_secondary)

def power_law_flat(min_bh_mass, max_bh_mass, max_mtotal=100, \
                            min_q=0.1, max_q=1.0, power_idx=-2.3):
    """
    Generate BBH masses. Primary is a power law (-2.3) in 5 - 50 and the secondary is generated according to a uniform distribution in mass ratio such that min_q < q < m_max_q.

    """
    assert 0 < min_q <= max_q
    assert min_q <= max_q <= 1
    # FIXME: Decouple this from astro_comp_mass
    # Done, but see if you can use the pareto distribution for rvs
    m1 = negative_power(min_bh_mass, max_bh_mass, power_idx)

    # FIXME: Make an option
    _min_q = max(min_q, min_bh_mass / m1)

    m2 = numpy.random.uniform(_min_q, max_q) * m1
    assert m1 + m2 <= max_mtotal
    return m1, m2

# Astrophysical object binary generators
uniform_bns_comp_mass = partial(uniform_comp_mass, min_primary=1.0, max_primary=2.0, min_secondary=1.0, max_secondary=2.0)
uniform_bbh_comp_mass = partial(uniform_comp_mass, min_primary=3.0, max_primary=100.0, min_secondary=3.0, max_secondary=100.0)
uniform_nsbh_comp_mass = partial(uniform_comp_mass, min_primary=3.0, max_primary=50.0, min_secondary=1.0, max_secondary=2.0)

# From Ozel and Freire (2016): https://arxiv.org/pdf/1603.02698.pdf
normal_bns_comp_mass = partial(normal_comp_mass, mean_primary=1.33, std_primary=0.09, mean_secondary=1.33, std_secondary=0.09)
astro_nsbh_comp_mass = partial(astro_comp_mass, min_primary=3.0, max_primary=50.0, mean_secondary=1.33, std_secondary=0.09, power_idx=-2.3)

_MASS_GENERATORS = {
    "delta_fcn_comp_mass": delta_fcn_comp_mass,
    "uniform_comp_mass": uniform_comp_mass,
    "normal_comp_mass": normal_comp_mass,
    "astro_comp_mass": astro_comp_mass,
    "power_law_flat": power_law_flat
}

# Spin generators
_spins_cart = ("spin1x", "spin1y", "spin1z", "spin2x", "spin2y", "spin2z")
def zero_spin():
    return 0., 0., 0., 0., 0., 0.

def uniform_aligned_spin(a1_max=1.0, a2_max=1.0):
    """
    Generate spins aligned with the orbital angular momentum. The spin magnitude is distributed uniformly up to maximum.
    """
    s1z, s2z = uniform(-a1_max, a1_max), uniform(-a2_max, a2_max)
    return 0., 0., s1z, 0., 0., s2z

def uniform_spin(a1_max=1.0, a2_max=1.0, a1_min=0.0, a2_min=0.0):
    """
    Generate isotropically distributed spins. The spin components magnitudes are uniform.
    """
    a1, a2 = uniform(a1_min, a1_max), uniform(a2_min, a2_max)
    th1, th2 = numpy.arcsin(uniform(-1, 1)), numpy.arcsin(uniform(-1, 1))
    ph1, ph2 = uniform(0, 2 * numpy.pi), uniform(0, 2 * numpy.pi)
    s1x, s1y, s1z = a1 * numpy.cos(th1) * numpy.cos(ph1), \
                    a1 * numpy.cos(th1) * numpy.sin(ph1), \
                    a1 * numpy.sin(th1)
    s2x, s2y, s2z = a2 * numpy.cos(th2) * numpy.cos(ph2), \
                    a2 * numpy.cos(th2) * numpy.sin(ph2), \
                    a2 * numpy.sin(th2)
    return s1x, s1y, s1z, s2x, s2y, s2z

def isotropic_spin(a1_max=1.0, a2_max=1.0, a1_min=0.0, a2_min=0.0):
    """
    Generate isotropically distributed spins. The spin components magnitudes are isotropic. E.g. this will favor large spin magnitudes.
    """
    m1 = (a1_max - a1_min)
    m2 = (a2_max - a2_min)
    # Note power ~ (idx - 1)
    a1, a2 = power(3) * m1 + a1_min, power(3) * m2 + a2_min
    th1, th2 = numpy.arcsin(uniform(-1, 1)), numpy.arcsin(uniform(-1, 1))
    ph1, ph2 = uniform(0, 2 * numpy.pi), uniform(0, 2 * numpy.pi)
    s1x, s1y, s1z = a1 * numpy.cos(th1) * numpy.cos(ph1), \
                    a1 * numpy.cos(th1) * numpy.sin(ph1), \
                    a1 * numpy.sin(th1)
    s2x, s2y, s2z = a2 * numpy.cos(th2) * numpy.cos(ph2), \
                    a2 * numpy.cos(th2) * numpy.sin(ph2), \
                    a2 * numpy.sin(th2)
    return s1x, s1y, s1z, s2x, s2y, s2z

def isotropic_aligned_spin(a1_max=1.0, a2_max=1.0):
    """
    Generate spins aligned with the orbital angular momentum. The spin magnitude is distributed as if the spin vectors were isotropic but the in-plane spin component is ignored.
    """
    _, _, s1z, _, _, s2z = isotropic_spin(a1_max, a2_max)
    return 0., 0., s1z, 0., 0., s2z

# A few representative spin distributions
# NSBH, NS non-spinning
astro_spin = partial(uniform_spin, a2_max=0.0)

#
# Post filtering
#
def mchirp_eta(event):
    return Mceta(event.mass1, event.mass2)

def mass_ratio(event):
    return min(event.mass1, event.mass2) / max(event.mass1, event.mass2)

# NOTE: redefines mass
_redshifted_masses = ("mass1", "mass2", "mass1_source", "mass2_source")
def detector_masses(event):
    return (event.mass1 * (1 + event.z), event.mass2 * (1 + event.z), \
            event.mass1, event.mass2)

def from_const_spin_params(event):
    pass

_chi_params = ("chi_eff", "chi_p")
def chi_params(event):
    chi_eff = (event.mass1 * event.spin1z + event.mass2 * event.spin2z) / (event.mass1 + event.mass2)
    s1_ip = numpy.sqrt(event.spin1x**2 + event.spin1y**2)
    s2_ip = numpy.sqrt(event.spin2x**2 + event.spin2y**2)
    chi_p = (event.mass1 * s1_ip + event.mass2 * s2_ip) / (event.mass1 + event.mass2)
    return chi_eff, chi_p

_spins_const = ("theta_jn", "phi_jl", "theta_1", "theta_2", "phi_12", "chi_1", "chi_2")
# Defined in Farr, et al. 2014
# https://journals.aps.org/prd/abstract/10.1103/PhysRevD.90.024018
# Transformation code stolen from bayespputils
# http://software.ligo.org/docs/pylal/pylal.bayespputils-pysrc.html
#def _inj_spins(self, inj, frame='OrbitalL'):
def const_spin_params(event, f_ref=20.):
    # FIXME: Deprecate these when possible.
    # geomtric utils
    from pylal.bayespputils import cart2sph, sph2cart, \
                                   array_ang_sep, array_polar_ang, \
                                   ROTATEZ, ROTATEY
    # physics/PN utils
    from pylal.bayespputils import orbital_momentum

    # FIXME: Hardcoded frame axis
    axis = lalsimulation.SimInspiralGetFrameAxisFromString("OrbitalL")

    m1, m2 = event.mass1, event.mass2
    mc, eta = Mceta(m1, m2)

    # Convert to radiation frame
    iota, s1x, s1y, s1z, s2x, s2y, s2z = \
        lalsimulation.SimInspiralInitialConditionsPrecessingApproxs( \
          event.inclination, event.spin1x, event.spin1y, event.spin1z, \
          event.spin2x, event.spin2y, event.spin2z, \
          m1 * lal.MSUN_SI, m2 * lal.MSUN_SI, \
          f_ref, event.coa_phase, axis
        )

    a1, theta1, phi1 = cart2sph(s1x, s1y, s1z)
    a2, theta2, phi2 = cart2sph(s2x, s2y, s2z)

    spins = {'chi_1': a1, 'theta_1': theta1, 'phi_1': phi1,
             'chi_2': a2, 'theta_2': theta2, 'phi_2': phi2,
             'iota': iota}

    # If spins are aligned, save the sign of the z-component
    if event.spin1x == event.spin1y == event.spin2x == event.spin2y == 0.:
        spins['a1z'] = event.spin1z
        spins['a2z'] = event.spin2z

    L  = orbital_momentum(f_ref, m1, m2, iota)
    S1 = numpy.hstack((s1x, s1y, s1z))
    S2 = numpy.hstack((s2x, s2y, s2z))

    zhat = numpy.array([0., 0., 1.])
    aligned_comp_spin1 = numpy.dot(S1, zhat)
    aligned_comp_spin2 = numpy.dot(S2, zhat)
    chi = aligned_comp_spin1 + aligned_comp_spin2 + \
          numpy.sqrt(1. - 4. * eta) * (aligned_comp_spin1 - aligned_comp_spin2)

    spins['spinchi'] = chi

    S1 *= m1**2
    S2 *= m2**2
    J = L + S1 + S2

    tilt1 = array_ang_sep(L, S1)
    tilt2 = array_ang_sep(L, S2)
    beta  = array_ang_sep(J, L)

    spins['tilt1'] = tilt1
    spins['tilt2'] = tilt2
    spins['beta'] = beta

    # Need to do rotations of
    # XLALSimInspiralTransformPrecessingInitialCondition inverse order to go in
    # the L frame
    # first rotation: bring J in the N-x plane, with negative x component
    phi0 = numpy.arctan2(J[1], J[0])
    phi0 = numpy.pi - phi0

    J = ROTATEZ(phi0, J[0], J[1], J[2])
    L = ROTATEZ(phi0, L[0], L[1], L[2])
    S1 = ROTATEZ(phi0, S1[0], S1[1], S1[2])
    S2 = ROTATEZ(phi0, S2[0], S2[1], S2[2])

    # now J in in the N-x plane and form an angle theta_jn with N, rotate by -theta_jn around y to have J along z
    theta_jn = array_polar_ang(J)
    spins['theta_jn'] = theta_jn

    J = ROTATEY(theta_jn, J[0], J[1], J[2])
    L = ROTATEY(theta_jn, L[0], L[1], L[2])
    S1 = ROTATEY(theta_jn, S1[0], S1[1], S1[2])
    S2 = ROTATEY(theta_jn, S2[0], S2[1], S2[2])

    # J should now be || z and L should have a azimuthal angle phi_jl
    phi_jl = numpy.arctan2(L[1], L[0])
    phi_jl = numpy.pi - phi_jl
    spins['phi_jl'] = phi_jl

    # bring L in the Z-X plane, with negative x
    J = ROTATEZ(phi_jl, J[0], J[1], J[2])
    L = ROTATEZ(phi_jl, L[0], L[1], L[2])
    S1 = ROTATEZ(phi_jl, S1[0], S1[1], S1[2])
    S2 = ROTATEZ(phi_jl, S2[0], S2[1], S2[2])

    theta0 = array_polar_ang(L)
    J = ROTATEY(theta0, J[0], J[1], J[2])
    L = ROTATEY(theta0, L[0], L[1], L[2])
    S1 = ROTATEY(theta0, S1[0], S1[1], S1[2])
    S2 = ROTATEY(theta0, S2[0], S2[1], S2[2])

    # The last rotation is useless as it won't change the differenze in spins'
    # azimuthal angles
    phi1 = numpy.arctan2(S1[1], S1[0])
    phi2 = numpy.arctan2(S2[1], S2[0])
    if phi2 < phi1:
        phi12 = phi2 - phi1 + 2. * numpy.pi
    else:
        phi12 = phi2 - phi1
    spins['phi_12'] = phi12

    return [spins[k] for k in _spins_const]

#
# Fiducial event constants
#
_tmplt_args = {
    "inclination": 0.0,
    "distance": 1.,
    "eccentricity": 0.0,
    "coa_phase": 0.0,
    "polarization": 0.0,
    "nonGRparams": None,
    "mass1": None,
    "mass2": None,
    "spin1x": 0.0,
    "spin1y": 0.0,
    "spin1z": 0.0,
    "spin2x": 0.0,
    "spin2y": 0.0,
    "spin2z": 0.0,
}

#
# Event and EventGenerator classes
#
class Event(object):
    def __init__(self):
        self.hp, self.hx = None, None

    @staticmethod
    def event_template(**kwargs):
        _t = _tmplt_args.copy()
        _t.update(kwargs)

        tmplt = Event()
        for k, v in list(_t.items()):
            setattr(tmplt, k, v)

        return tmplt


    def del_waveform(self):
        self.hp, self.hx = None, None

    def gen_waveform(self, flow=10.0, deltaf=0.125, fhigh=2048., fref=10., approximant="IMRPhenomPv2"):
        """
        Generate, and internally store, the h_+ and h_x polarizations for an event.
        """
        params = None

        try:
            self.hp, self.hx = lalsimulation.SimInspiralFD(
                    self.mass1 * lal.MSUN_SI, self.mass2 * lal.MSUN_SI,
                    self.spin1x, self.spin1y, self.spin1z,
                    self.spin2x, self.spin2y, self.spin2z,
                    self.distance * 1e6 * lal.PC_SI, self.inclination,
                    self.coa_phase, 0.0, self.eccentricity, 0.0,
                    deltaf, flow, fhigh, fref, params,
                    lalsimulation.SimInspiralGetApproximantFromString(approximant))
        except RuntimeError as e:
            print(e)
            print(( self.mass1, self.mass2,
                    self.spin1x, self.spin1y, self.spin1z,
                    self.spin2x, self.spin2y, self.spin2z,
                    self.distance, self.inclination,
                    self.coa_phase, 0.0, self.eccentricity, 0.0,
                    deltaf, flow, fhigh, fref, params, approximant))
            raise e

        return self.hp.data.data + self.hx.data.data

class SingleInstrumentEvent(SingleEvent):
    """
    Mostly for Bayestar compatibility.
    """
    def __init__(self, d, s, p, t, zt, psd):
        super(SingleEvent, self).__init__()
        self._detector = d
        self._instrument = d
        self._snr = s
        self._phase = p
        self._time = t
        self._zerolag_time = zt
        self._psd = psd

    @property
    def detector(self):
        return self._detector

    @property
    def instrument(self):
        return self._instrument

    @property
    def snr(self):
        return self._snr

    @property
    def phase(self):
        return self._phase

    @property
    def time(self):
        return self._time

    @property
    def zerolag_time(self):
        return self._zerolag_time

    @property
    def psd(self):
        return self._psd

    @property
    def snr_series(self):
        return self._snr_series

class NetworkEvent(GenericEvent):
    """
    Mostly for Bayestar compatibility.
    """

    @property
    def singles(self):
        return self._singles

    @property
    def template_args(self):
        return self._template_args

    def __init__(self, event, network):
        self.event = event
        self.network = network
        self._singles = []
        self._template_args = dict((a, getattr(self.event, a)) for a in dir(self.event))

        from netsim import _to_lal_real_fs
        for i, n in self.network.snr(self.event).items():
            phoa = self.network.phase_on_arrival(self.event, (i,))
            _e = SingleInstrumentEvent(i, n, phoa, event.event_time, None, \
                    _to_lal_real_fs(self.network._instr[i]))
            self.singles.append(_e)

class EventGenerator(object):
    def __init__(self):
        # These are functions called to generate properties of an event.
        # The keys are mapped to values returned by the function.
        self._generators = {}

        # These are functions called to generate depedent properties of an
        # event. That is, properties that are fixed by other generated
        # properties, such as chirp mass, etc...
        # The keys are mapped to values returned by the function.
        # We use an ordered dictionary here in case some values overwrite
        # others --- this ensures that the overwriting order is preserved
        self._post_filters = OrderedDict()

        # These are functions called to do rejection sampling of generated
        # events.
        self._conditionals = []

        self._events = []

    def __contains__(self, param):
        return param in self.get_attributes()

    def __getitem__(self, param):
        if param not in self:
            raise KeyError("Parameter {0} not in attributes.".format(param))
        return numpy.asarray([getattr(e, param) for e in self._events])

    def __iter__(self):
        for e in self._events:
            yield e

    def __len__(self):
        return len(self._events)

    def append(self, egen):
        # FIXME: might be possible to extend with events that do not have the
        # same set of attributes. Should check for that.
        self._events.extend(egen._events)

    def append_generator(self, lbls, gen, **kwargs):
        """
        Add a generator for a set of properties of the event. Propety names are specified by `lbls`, the generator function is `gen`, and `kwargs` is bound to the function via `functools.partial`. This is useful if the generator function itself has arguments which need to be set, e.g. maximum masses, ranges, etc...
        """
        try:
            _ = iter(lbls)
        except TypeError:
            raise Exception("lbls argument must be iterable, given: {0}".format(str(lbls)))

        if kwargs:
            self._generators[lbls] = partial(gen, **kwargs)
        else:
            self._generators[lbls] = gen

    def append_post(self, lbls, flt, **kwargs):
        """
        Add a post-generator function for a set of properties of the event. Typically, this is for properties which are not controlled by the generator function, but are instead derived (e.g. total mass from component masses). Propety names are specified by `lbls`, the post function is `flt`, and `kwargs` is bound to the function via `functools.partial`. This is useful if the post filter function itself has arguments which need to be set.
        """
        try:
            _ = iter(lbls)
        except TypeError:
            raise Exception("lbls argument must be iterable, given: {0}".format(str(lbls)))

        try:
            _ = iter(lbls)
        except TypeError:
            raise Exception("lbls argument must be iterable, given: {0}".format(str(lbls)))

        if kwargs:
            self._post_filters[lbls] = partial(flt, **kwargs)
        else:
            self._post_filters[lbls] = flt

    def append_conditional(self, cond, **kwargs):
        """
        Add a conditional function when generating properties of the event. This is useful to allow for a type of rejection sampling. An example would be rejecting events whose SNR falls below a certain threshold. The conditional function is `cond`, and `kwargs` is bound to the function via `functools.partial`. This is useful if the conditional itself has arguments which need to be set.
        """
        if kwargs:
            self._conditionals.append(partial(cond, **kwargs))
        else:
            self._conditionals.append(cond)

    def generate_events(self, n_itr=None, n_event=None, verbose=False, \
                                yield_all=False):
        """
        Loop and generate events.
        """

        # Just loop forever
        if n_itr is None:
            n_itr = float("inf")

        i, tries = 0, 0
        while i < n_itr:
            tries += 1
            event = Event()

            #if verbose is True or tries % verbose == 0:
            if verbose:
                print("{0:d} / {1}: {2:d} / {3}".format(i, str(n_itr), \
                                            len(self._events), str(n_event)))

            # Generate user-specified randomized properties
            for lbl, gen in self._generators.items():
                try:
                    rv = gen()
                    for l, v in zip(lbl, rv):
                        setattr(event, l, v)
                except TypeError as e:
                    print("Got exception in generation step for labels: {0}".format(lbl))
                    raise e

            # Generate "dependent" properties
            for lbl, trans in self._post_filters.items():
                try:
                    rv = gen()
                    for l, v in zip(lbl, trans(event)):
                        setattr(event, l, v)
                except TypeError as e:
                    print("Got exception in post step for labels: {0}, and result {1}.".format(lbl, rv))
                    raise e

            # Check that the event passes checks
            check = True
            for func in self._conditionals:
                check &= func(event)

            if yield_all:
                yield check, event
            elif check:
                yield event

            if not check:
                continue

            self._events.append(event)

            i += 1

            if n_event and len(self._events) >= n_event:
                break

        if verbose:
            print("%d events generated, %d kept." % (tries, len(self._events)))

    def clear_events(self):
        self._events = []

    def reapply_post(self):
        """
        This reapplies all post filters on the event set. This may be useful in cases where the event list is statically generated, and the user wants a new set of columns that wasn't accessible during generation.
        """
        # Regenerate "dependent" properties
        for event in self._events:
            for lbl, trans in self._post_filters.items():
                for l, v in zip(lbl, trans(event)):
                    setattr(event, l, v)

    @staticmethod
    def from_pos_file(fname):
        e = EventGenerator()
        e.load_events_from_ascii(fname)
        return e

    def load_events_from_ascii(self, fname, idx=None):
        """
        Load a set of events generated from an ASCI table with a header. This is accomplished by an internal generator method which 'replays' the generated events from the file.
        """

        data = numpy.genfromtxt(fname, names=True)
        attrs = tuple(data.dtype.names)
        def _gen():
            if len(self._events) == len(data[attrs[0]]):
                raise StopIteration()
            return [data[a][len(self._events)] for a in attrs]

        alias_columns = ("event_time", "declination", "right_ascension",
                        "mass1", "mass2", "phi_orb", "coa_phase",
                        "eccentricity", "polarization")
        def post_filter(e):
            return (e.time, e.dec, e.ra, e.m1, e.m2, 0., 0., 0., e.psi)
        self.append_post(alias_columns, post_filter)

        spin_columns = ("inclination",
                        "spin1x", "spin1y", "spin1z",
                        "spin2x", "spin2y", "spin2z",)
        def spin_conv(e):
            # Aligned spin
            if not hasattr(e, "phi_jl"):
                return e.theta_jn, 0.0, 0.0, e.a1z, 0.0, 0.0, e.a2z

            return lalsimulation.SimInspiralTransformPrecessingNewInitialConditions( \
                e.theta_jn, e.phi_jl, e.tilt1, e.tilt2, e.phi12,
                e.a1, e.a2, e.m1, e.m2, e.f_ref, 0.) #e.phi_orb)
        self.append_post(spin_columns, spin_conv)

        source_columns = ("redshift", "m1_source", "m2_source")
        def source_conv(e):
            return (e.redshift, e.m1_source, e.m2_source)
        self.append_post(source_columns, source_conv)

        self.append_generator(attrs, _gen)
        n_events = len(data[attrs[0]])

        # This basically "replays" the events
        for _ in self.generate_events(n_itr=n_events):
            pass

    @staticmethod
    def from_xml(fname):
        e = EventGenerator()
        e.load_events_from_xml(fname)
        return e

    def load_events_from_xml(self, fname):
        """
        Load a set of events from a LIGOLW XML file. Must contain one and only one sim table.
        FIXME: Should use "replay" strategy a la the HDF5 version.
        """

        from glue.ligolw import lsctables, utils, ligolw
        lsctables.use_in(ligolw.LIGOLWContentHandler)

        xmldoc = utils.load_filename(fname, contenthandler=ligolw.LIGOLWContentHandler)
        tbl = lsctables.SimInspiralTable.get_table(xmldoc)
        attrs = set(tbl.validcolumns.keys())
        for xmlevent in tbl:
            e = Event()
            for k in list(attrs):
                try:
                    setattr(e, k, getattr(xmlevent, k))
                except AttributeError:
                    # FIXME: on the off chance we get heterogenous data, this
                    # will prevent this attribute from being retrieved later
                    attrs.remove(k)
                    continue
            e.eccentricity = 0.
            e.right_ascension = e.longitude
            e.declination = e.latitude
            e.event_time = e.geocent_end_time + 1e-9 * e.geocent_end_time_ns
            self._events.append(e)

        self._idx = 0
        attrs.add("eccentricity")
        attrs.add("right_ascension")
        attrs.add("declination")
        attrs.add("event_time")
        attrs.remove("latitude")
        attrs.remove("longitude")
        attrs.remove("geocent_end_time")
        attrs.remove("geocent_end_time_ns")

        # FIXME: hack around getting attribute names
        self.append_post(tuple(attrs), lambda e: None)

    def to_xml(self, fname, add_all_cols=False, verbose=False):
        """
        Dump the current event set to a LIGOLW XML file.
        """
        _mapping = {
            "longitude": "right_ascension",
            "latitude": "declination"
        }

        from glue.ligolw import lsctables, utils, ligolw, ilwd
        from glue.ligolw.utils import process as lw_process
        lsctables.use_in(ligolw.LIGOLWContentHandler)

        xmldoc = ligolw.Document()
        xmldoc.appendChild(ligolw.LIGO_LW())

        proc_table = lw_process.register_to_xmldoc(xmldoc, __name__, {})
        proc_id = proc_table.process_id

        dump_attrs = set(lsctables.SimInspiralTable.validcolumns.keys()) & \
            self.get_attributes()
        dump_attrs = list(dump_attrs)
        dump_attrs += _mapping
        addl_attrs = ["simulation_id", "process_id", "geocent_end_time", \
                        "geocent_end_time_ns"]

        if add_all_cols:
            dump_attrs += lsctables.SimInspiralTable.validcolumns.keys()
            dump_attrs = list(set(dump_attrs))

        sim_table = lsctables.New(lsctables.SimInspiralTable, \
                                    dump_attrs + addl_attrs)
        for e in self._events:
            row = sim_table.RowType()
            row.process_id = proc_id
            row.simulation_id = sim_table.get_next_id()
            _t = lal.LIGOTimeGPS(e.event_time)
            row.geocent_end_time = _t.gpsSeconds
            row.geocent_end_time_ns = _t.gpsNanoSeconds

            for a in dump_attrs:
                try:
                    if a in _mapping:
                        setattr(row, a, getattr(e, _mapping[a]))
                    else:
                        setattr(row, a, getattr(e, a))
                except AttributeError:
                        setattr(row, a, None)

            sim_table.append(row)

        xmldoc.childNodes[0].appendChild(sim_table)
        utils.write_filename(xmldoc, fname, gz=fname.endswith("gz"))

    def to_hdf5(self, fname, root=None, verbose=False, meta=None):
        with h5py.File(fname, "a") as hfile:
            try:
                grp = hfile[root]
            except:
                grp = hfile.create_group(root)

            if meta is not None:
                for k, v in list(meta.items()):
                    grp.attrs[k] = v

            for lbl in self.get_attributes():
                try:
                    dat = numpy.asarray([getattr(e, lbl) for e in self._events])
                except AttributeError:
                    print("Unable to retrieve data for attribute {0}, skipping".format(lbl), file=sys.stderr)
                    continue
                if numpy.issubdtype(dat.dtype, numpy.number):
                    ds = grp.create_dataset(lbl, data=dat)
                else:
                    if verbose:
                        print("Skipping non-serializable column %s" % lbl)

        # TODO: could write SNRs and whatnot

    def load_events_from_hdf5(self, fname, idx=None, root=None):
        """
        Load a set of events generated and serialized into HDF5 from a previous instance of an EventGenerator. This will reinitialize any of the fields that were saved in the previous generation run (via checking the Dataset names).
        This is accomplished by an internal generator method which 'replays' the generated events from the HDF5 file. This may be slower than you were hoping, and there's no way to recovery the individual sets of parameters or their generator functions from the previous run.
        """

        with h5py.File(fname) as hfile:
            grp = hfile[root or "/events/"]
            attrs = tuple([a for a in list(grp.keys()) if grp[a].shape])

            # Copy into memory since individual indexing is sloooooow
            memmap = dict([(a, grp[a][:]) for a in attrs])

            def _gen():
                if len(self._events) == len(grp[attrs[0]]):
                    raise StopIteration()
                return [memmap[a][len(self._events)] for a in attrs]
            self.append_generator(attrs, _gen)
            n_events = len(grp[attrs[0]])

            # This basically "replays" the events
            for _ in self.generate_events(n_itr=n_events):
                pass

    @staticmethod
    def from_hdf5(fname, root=None):
        e = EventGenerator()
        e.load_events_from_hdf5(fname, root=root)
        return e

    def get_attributes(self):
        attrs = set()

        for lbls in self._generators:
            attrs |= set(lbls)
        for lbls in self._post_filters:
            attrs |= set(lbls)

        return attrs

    def to_dataframe(self):
        from pandas import DataFrame
        return DataFrame.from_records( \
            list(map(dict, [[(attr, getattr(e, attr)) for attr in self.get_attributes()] for e in self._events])))

def plot(eventgen, fname=None, select=None, params=None, **kwargs):
    import matplotlib
    matplotlib.use("agg")
    from matplotlib import pyplot
    from corner import corner

    # FIXME: Shim code, basic emulation of Param from the pe_page code
    class _Param(object):
        def __init__(self, name):
            self.name = name

        def __call__(self, e):
            return e[self.name]

    attrs = params or eventgen.get_attributes()
    data = numpy.empty((len(attrs), len(eventgen)))
    for i, a in enumerate(attrs):
        if isinstance(a, str):
            a = _Param(a)
        data[i] = a(eventgen)

    # Suppress elements with no dynamic range
    suppress = list()
    for i, d in enumerate(data):
        if len(numpy.unique(d)) != 1:
            suppress.append(i)

    attrs = [a for i, a in enumerate(attrs) if i in suppress]
    fig = corner(data[numpy.asarray(suppress)].T, labels=list(attrs), \
                    weights=select, **kwargs)
    if fname is not None:
        pyplot.savefig(fname)

    return fig

if __name__ == "__main__":
    import doctest
    doctest.testmod()
