#!/usr/bin/env python
"""
Localize a set of gravitational-wave binary events.
"""
import os
import argparse

import numpy

from lalinference.io import fits

import eventgen
from netsim import Network, list_instruments
import kilonovae
import em_bright

def save_light_curve(t, lc, freq, flux, meta, fname, root="/light_curves/"):
    import h5py
    with h5py.File(fname, "w") as hfile:
        try:
            grp = hfile[root]
        except:
            grp = hfile.create_group(root)
        grp.attrs["event_time"] = meta["event_time"]
        grp.attrs["ejecta_mass_units"] = "solar mass"
        grp.attrs["ejecta_mass"] = meta["ejecta_mass"]
        grp.attrs["distance_units"] = "Mpc"
        grp.attrs["distance"] = meta["distance"]

        ds = grp.create_dataset("freq", data=freq)
        ds.attrs["freq_units"] = "Hz"

        # FIXME: actually luminosity spectrum
        ds = grp.create_dataset("flux", data=flux)
        ds.attrs["flux_units"] = "erg / s / Hz"

        ds = grp.create_dataset("times", data=t)
        ds.attrs["time_units"] = "seconds"

        for band, mags in lc.iteritems():
            ds = grp.create_dataset("%s_light_curve" % band, data=mags)
            ds.attrs["bandpass"] = band

_network = ("H1", "L1", "V1")
argp = argparse.ArgumentParser()
argp.add_argument("-w", "--write-output", default="", help="Write output to this filename stem.")
argp.add_argument("-B", "--no-bayestar", action="store_true", help="Skip GW localization.")
argp.add_argument("-L", "--no-light-curve", action="store_true", help="Skip kilonovae light curve generation.")
argp.add_argument("-o", "--no-overwrite", default=False, action="store_true", help="Do not overwrite files if they've already been generated.")
argp.add_argument("-v", "--verbose", action="store_true", help="Talk lots.")
argp.add_argument("hdf5_name", help="HDF5 to draw events from")

instr = argp.add_argument_group("Network Options", "Options controlling the GW network configuration.")
instr.add_argument("-l", "--list-instruments", action="store_true", help="List all GW instruments available and their shorthand.")
instr.add_argument("-n", "--network", help="Network of instruments to use for antenna based calculations. Default is %s" % ", ".join(_network))
#argp.add_argument("-p", "--psd", action='append', help="For each instrument, override the power spectral density (sensitivity) from its default. The default is to use the fully advanced design sensitivity \"2018-era\" sensitivity. Can be provided several times: \"-p H1=spec -p V1=otherspec\"")
args = argp.parse_args()

#
# Argument parsing
#
if args.list_instruments:
    list_instruments()
    exit(0)

if args.write_output is None:
    exit("Need a filename to write to.")

if args.network is None:
    inetwork = _network
else:
    inetwork = args.network.split(", ")

network = Network()
for i in inetwork:
    network.add_psd(i)

bname, _ = os.path.splitext(args.write_output)

event_list = eventgen.EventGenerator.from_hdf5(args.hdf5_name)
for i, e in enumerate(event_list._events):
    net_snr = network.snr(e)

    print "Event (%d) at %10.9f has SNRs %s" % (i, e.event_time, str(net_snr))

    if not args.no_bayestar:
        print "Localizing..."
        smap_fname = "%s.%d.fits.gz" % (bname, i)
        # FIXME: Enable for testing
        #network.test_phase(e, "H1")
        if not (args.no_overwrite and os.path.exists(smap_fname)):
            smap = network.bs_localize(e)
            print "Done, saving to %s" % smap_fname
            fits.write_sky_map(smap_fname, smap, nest=True)
        else:
            print "%s already exists, skipping" % smap_fname

    if not args.no_light_curve:
        print "Generating light curve"
        chi_bh = numpy.sqrt(e.spin1x**2 + e.spin1y**2 + e.spin1z**2)
        # FIXME: need a nominal NS radius
        m_ej = em_bright.frac_disk_mass(e.mass1, e.mass2, chi_bh) * e.mass2
        m_ej = max(0, m_ej)
        t, flux = kilonovae.generate_light_curve(m_ej * kilonovae.MSUN_CGS)
        # frequency independent metadata
        meta = {
            "ejecta_mass": m_ej,
            "distance": e.distance,
            "event_time": e.event_time,
        }

        # FIXME: move standard generation to photometry module
        freq = kilonovae.C_CGS / numpy.logspace(-5, -3, 10000)

        from photometry import _bands
        lc = {}
        for bnd in _bands:
            mag = kilonovae.bandlimited_mag_at_earth(flux, band=bnd, \
                        dist=e.distance * 1e6 * kilonovae.PC_CGS)
            lc[bnd] = mag

        lc_fname ="%s.%d.h5" % (bname, i)
        save_light_curve(t, lc, freq, flux, meta, lc_fname)
