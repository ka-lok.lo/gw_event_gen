from lalinference.io import fits
import healpy
import numpy as np


def create_table(fitsfile):
    #import pdb; pdb.set_trace()
    skymap, _ = fits.read_sky_map(fitsfile)
    dat = np.zeros((4, len(skymap)))
    ns = healpy.npix2nside(len(skymap))
    dec, ra = healpy.pix2ang(ns, np.asarray(range(len(skymap))))
    dat[0,:] = ra * 12 / np.pi
    dat[1,:] = dec * 180 / np.pi
    dat[2,:] = skymap
    dat = dat[:,np.argsort(dat[2,:])[::-1]]
    dat[3,:] = np.cumsum(dat[2,:])
    dat = np.rec.fromarrays(dat, names=("ra", "dec", "prob", "cumul"))
    return dat, skymap
