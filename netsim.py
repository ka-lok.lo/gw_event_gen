from __future__ import print_function
import os
import math

import numpy
from scipy.interpolate import interp1d
from scipy.integrate import trapz
from scipy.optimize import brentq

import lal
import collections
_detectors = dict([(d.frDetector.prefix, d) for d in lal.CachedDetectors])
import lalsimulation

import eventgen

#
# Analytic volume utilities
#

# Table I, Finn and Chernoff, 1993
_theta_sq = numpy.transpose([(float(l[1]) / 16., float(l[0]) / 100.) for l in \
[_.strip().split() for _ in \
"""100.0 0.0
90.0 0.240
80.0 0.542
75.0 0.707
70.0 0.878
60.0 1.250
50.0 1.709
40.0 2.283
30.0 3.020
25.0 3.485
20.0 4.063
10.0 6.144
9.0  6.471
8.0  6.832
7.0  7.239
6.0  7.701
5.0  8.233
4.0  8.857
3.0  9.614
2.0  10.589
1.0  11.985
0.5  13.054
0.4  13.350
0.3  13.682
0.2  14.091
0.1  14.284
0.0  16.000""".split("\n")]])
_theta_sq = interp1d(*_theta_sq)

def _fit_theta(values, theta_sq_intrp=_theta_sq):
    vmin, vmax = numpy.min(values), numpy.max(values)
    _val_scaled = (values - vmin) / (vmax - vmin)
    # CDF already has 16 scaled out
    return theta_sq_intrp(_val_scaled**2)

#
# Mock PSDs and PSD parsing
#

# Apply fudge factor of 2 (in ASD) to mock up A+
def aplus_mock_psd(freq):
    return lalsimulation.SimNoisePSDaLIGOZeroDetHighPower(freq) / 4

_MOCK_PSDS = {
    "aplus_mock_psd": aplus_mock_psd,
}

# FIXME: Switch to lalsimulation version when it becomes available
try:
    d = os.path.dirname(os.path.abspath(__file__))
    fname = os.path.join(d, "LIGO-T1800042-v5-aLIGO_APLUS.txt")
    f, a = numpy.loadtxt(fname, unpack=True)
    _APLUS_INTERP = interp1d(f, a**2)
    def aplus_design_psd(freq):
        return _APLUS_INTERP(freq)

    _MOCK_PSDS["aplus_design_psd"] = aplus_design_psd
except IOError:
    pass

_default_psds = {
    "H1": lalsimulation.SimNoisePSDaLIGOZeroDetHighPower,
    "I1": lalsimulation.SimNoisePSDaLIGOZeroDetHighPower,
    "K1": lalsimulation.SimNoisePSDKAGRA,
    "L1": lalsimulation.SimNoisePSDaLIGOZeroDetHighPower,
    "V1": lalsimulation.SimNoisePSDAdvVirgo,
    None: lalsimulation.SimNoisePSDaLIGOZeroDetHighPower
}

def parse_psd(name, freqar=None):
    freqar = numpy.arange(0, 2048.125, 0.125) if freqar is None else freqar

    if name in dir(lalsimulation):
        fcn = getattr(lalsimulation, name)
        # No way to get the arity of builtins
        # So... guess and check
        try:
            # Prebuilt mapping
            p = list(map(fcn, freqar))
        except TypeError:
            # fill from file
            psdar = _construct_psd(numpy.zeros(freqar.shape), freqar[1] - freqar[0])
            fcn(psdar, freqar[0])
            p = psdar.data.data
    elif name in _MOCK_PSDS:
        p = list(map(_MOCK_PSDS[name], freqar))
    elif os.path.exists(name):
        f, p = numpy.loadtxt(name, unpack=True)
        p = interp1d(f, p)(freqar)
    else:
        raise ValueError("No pregenerated PSD or PSD file found for %s" % name)
    return numpy.asarray(p)

def list_instruments():
    for prefix in sorted(_detectors):
        inst = _detectors[prefix].frDetector.name
        psd = \
            _default_psds[prefix if prefix in _default_psds else None].__name__
        print("%s (%s): default PSD: %s" % (inst, prefix, psd))

def _to_lal_cmplx_fs(np_ar):
    _unit = lal.StrainUnit * lal.SecondUnit
    fs = lal.CreateCOMPLEX16FrequencySeries("", lal.LIGOTimeGPS(0.0), 0., 0.125, _unit, len(np_ar))
    fs.data.data = np_ar.copy()
    return fs

def _to_lal_real_fs(np_ar):
    _unit = lal.StrainUnit ** 2 * lal.SecondUnit
    fs = lal.CreateREAL8FrequencySeries("", lal.LIGOTimeGPS(0.0), 0., 0.125, _unit, len(np_ar))
    fs.data.data = np_ar.copy()
    return fs

def _construct_psd(data, delta_f):
    psd = lal.CreateREAL8FrequencySeries('', lal.LIGOTimeGPS(0), 0., delta_f,  lal.StrainUnit, len(data))
    psd.data.data = data
    return psd

def _construct_psd_from_func(func, delta_f, fhigh):
    return _construct_psd(list(map(func, numpy.linspace(0, fhigh, delta_f))), delta_f)

class Network(object):
    def __init__(self, instruments=tuple()):
        """
        Initialize the object, optionally providing instrument prefixes which will be added to the object with their default PSDs.
        """
        self._instr = {}
        self._psd_cache = {}
        for inst in instruments:
            self.add_psd(inst)

    def __repr__(self):
        return ", ".join(list(self._instr.keys()))

    """
    def get_psd(self, ifo)
        if type == func:
            pass
        return
    """

    def add_psd(self, inst, psd=None, delta_f=0.125):
        """
        Add an instrument to network characterized with a given PSD.
        """
        if psd is None:
            psd = numpy.asarray(list(map(_default_psds[inst], numpy.arange(0, 2048.125, 0.125))))
        elif isinstance(psd, collections.Callable):
            psd = numpy.asarray(list(map(psd, numpy.arange(0, 2048.125, 0.125))))
        else:
            psd = parse_psd(psd, numpy.arange(0, 2048.125, 0.125))

        self._instr[inst] = psd

    def del_psd(self, inst):
        """
        Remove an instrument from the network.
        """
        del self._inst[inst]

    def _invalidate_psd_cache(self):
        self._psd_cache = {}

    def bs_localize(self, event, ifos=None, waveform="IMRPhenomPv2pseudoFourPN", fmin=10., fmax=2048., verbose=True):
        """
        Ship this event off to BAYESTAR to be localized.
        """
        from lalinference.bayestar import sky_map
        if verbose:
            import logging
            logging.basicConfig(level=logging.INFO)
            log = logging.getLogger('BAYESTAR')
        netevent = eventgen.NetworkEvent(event, self)
        return sky_map.localize(netevent, waveform, fmin, enable_snr_series=False)

    def phase_on_arrival(self, event, ifos=None):
        """
        Calculate event phase offsets for a given set of instruments. Derived from an expression in eq. 6 of Phys. Rev. D 93, 024013.
        """
        for ifo in ifos or self._instr:
            det = _detectors[ifo]
            gmst = lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(event.event_time))
            fp, fx = lal.ComputeDetAMResponse(det.response, \
                        event.right_ascension, event.declination, \
                        0., gmst)
            zeta = numpy.exp(-2j * event.polarization) * (fp + 1j * fx)
            pref = 0.5 * (1 + numpy.cos(event.inclination)**2) * zeta.real - 1j * numpy.cos(event.inclination) * zeta.imag #* numpy.exp(2j * event.coa_phase)
        #return numpy.angle(pref)
        return pref

    def test_phase(self, event, ifo):
        det = _detectors[ifo]
        gmst = lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(event.event_time))
        fp, fx = lal.ComputeDetAMResponse(det.response, \
                    event.right_ascension, event.declination, \
                    event.polarization, gmst)
        event.spin1x, event.spin1y, event.spin1z = 0., 0., 0.
        event.spin2x, event.spin2y, event.spin2z = 0., 0., 0.

        # We'll be manipulating these, so make sure you save them
        incl, coa_phase = event.inclination, event.coa_phase

        # Get "template"
        event.inclination, event.coa_phase = 0., 0.
        event.gen_waveform(flow=100., deltaf=1., fhigh=101., fref=100)#, approximant="TaylorF2")
        zi_hp, zi_hx = event.hp, event.hx
        zi_wf = zi_hp.data.data + 1j * zi_hx.data.data

        # normalize
        zi_wf *= 2. / numpy.sum(numpy.abs(zi_wf)**2)

        # restore inclination for "signal"
        event.inclination = incl
        event.gen_waveform(flow=100., deltaf=1., fhigh=101., fref=100)#, approximant="TaylorF2")

        hp, hx = event.hp, event.hx
        # apply antenna factors
        wf = fp * hp.data.data + fx * hx.data.data

        # "filter"
        expected = numpy.sum(numpy.conj(zi_wf) * wf)

        # get our expected result for the phase
        result = self.phase_on_arrival(event, (ifo,))

        # restore coalescence phase
        event.coa_phase = coa_phase

        # do the test
        assert numpy.isclose(expected, result)

    def horizon(self, snr_threshold=8, ifos=None, \
                    fmin=10., fmax=2048., **kwargs):
        """
        Determine the horizon distance: Euclidean distance at which a source with the given parameters, optimally oriented, for an event gives the snr value of snr_threshold.

        Does not correct for redshift, see `redshift_horizon` for this functionality.
        """
        # explicitly override distance to known value, just in case
        # FIXME: astropy units, please
        kwargs["distance"] = 1. # Mpc

        tmplt = eventgen.Event.event_template(**kwargs)
        horizon_dict = self.snr(tmplt, ifos, fmin, fmax, optimal=True)
        for inst in horizon_dict:
            snr = horizon_dict[inst]
            # Assumes distance factor is unity
            horizon_dict[inst] = snr / snr_threshold

        return horizon_dict

    def _theta_sq_net(self):
        """
        Compute the average amplitude distribution for the network.
        """
        from eventgen import theta
        rvs = []
        for _ in range(10000):
            ra, dec, incl, pol, _ = eventgen.random_orientation()
            # FIXME: probably not necessary?
            t = numpy.random.uniform(1e9, 1e9 + 86400 * 365)
            th_sq = numpy.square([theta(ifo, t, ra, dec, pol, incl) \
                                    for ifo in self._instr]).sum()
            rvs.append(th_sq)

        # FIXME: is scaling by number of detectors right?
        rvs = numpy.asarray(sorted(rvs)) / 16 / len(self._instr)

        rvs = numpy.concatenate(([0], rvs, [1]))
        return rvs, 1 - numpy.linspace(0, 1, len(rvs))


    def analytic_volume(self, snr_threshold=8, fmin=10., fmax=2048., **kwargs):
        """
        Fixed mass/spin analytic detection weighted sensitivity volume. Currently uses Planck15 cosmology from astropy. Returns volume in Gpc^3.
        """
        import sys
        from astropy.cosmology import Planck15
        from scipy.integrate import fixed_quad
        print("This function is mostly untested, and uses the horizon of the most sensitive detector rather than trying to figure out the correct horizon threshold. Its preliminary tests show agreement at the 10% level with other sources.", file=sys.stderr)
        # FIXME: use whole network
        #z_h = self.redshift_horizon(snr_threshold, fmin, fmax, **kwargs)
        z_h = max(self.redshift_horizon(snr_threshold, fmin=fmin, fmax=fmax, **kwargs).values())
        _4pi = 4 * numpy.pi
        def _intg(z):
            return _fit_theta(z) / (1 + z) * \
                        _4pi * Planck15.differential_comoving_volume(z).value
        return fixed_quad(_intg, 0, z_h)[0] / 1e9

    def pop_averaged_volume(self, distr_func, snr_threshold=8, fmin=10., \
                                fmax=2048., z_max=2, network=False, **kwargs):
        """
        Determine the redshift horizon for a population of events with a given snr_threshold. Requres an iterable for distr_func, in which each element of the iterable provides a set intrinsic parameters (currently just mass1 and mass2). The detection probability is assumed to follow the Finn & Chernoff theta^2 function CCDF.

        If network=False, this will use the horizon and detection probability functions for a single instrument (the horizon being the maximum over all instruments). If network=True, the snr_threshold is assumed to be the *network* SNR threshold, and the detection probability is recalculated to account for correlations between the instruments in the network.

        Currently uses Planck15 cosmology from astropy.
        """

        # Import here in case people don't have/want astropy
        from astropy import units
        from astropy.cosmology import Planck15, z_at_value

        u = numpy.linspace(0, z_max, 1000)

        # Volume factor
        dVdu = Planck15.differential_comoving_volume(u)
        dVdu = 4 * numpy.pi * dVdu.to("Gpc^3 / sr").value 
        #assert numpy.isclose(trapz(dVdu, u), \
                            #Planck15.comoving_volume(u[-1]).to("Gpc^3").value)

        # Find the expectation value of the redshifted detection function
        favg = numpy.zeros(u.shape)

        # Choose whether to use the (correlated) network amplitude distribution
        # or a single detector
        pdet = interp1d(*self._theta_sq_net()) if network else _theta_sq

        #
        # MC integral version
        #
        n = 0
        for e in distr_func:
            # For now, just masses
            # FIXME: Need to check the horizon for the network
            # FIXME: This is the slowest step in the loop --- could probably
            # use an interpolation table, but that will get messy in high
            # dimensions
            zh = max(self.redshift_horizon(mass1=e.mass1, mass2=e.mass2, \
                                        snr_threshold=snr_threshold).values())
            assert zh <= z_max, "Found an event with horizon redshift ({0:f}) beyond the assumed maximum ({1:f}). Masses: {2:.1f} {3:.1f}".format(zh, z_max, e.mass1, e.mass2)

            # What interval is our detection efficiency defined on
            valid = u < zh
            if len(u[valid]) / float(len(u)) < 0.01:
                print("Warning, volume calculation is using less than 1% of the total redshift range. Inaccuracies could result.")

            # Horizon-scaled detection probability: f(u * z_h / z_max)
            th_sq = _fit_theta(u[valid], pdet)
            rs_factor = 1 + u * zh / z_max

            # f(u * z_h) / (1 + u * z_max) * dVdu
            favg[valid] += th_sq / rs_factor[valid]

            n += 1

        favg *= dVdu
        favg /= n

        # If requested, clip the interval
        clip = kwargs.pop("z_max_clip", None)
        if clip is not None:
            clip = u < clip
            u, favg = u[clip], favg[clip]

        diff = kwargs.pop("differential", None)
        if diff is not None:
            return u, favg

        # final integral over scaled redshifts
        return trapz(favg, u)


    def redshift_horizon(self, snr_threshold=8, ifos=None, \
                            fmin=10., fmax=2048., **kwargs):
        """
        Determine the redshift horizon for an event with a given snr_threshold. This effectively searches for the redshift at which the combination of the redshifted masses and luminosity distance provide an SNR of 8 in a given instrument. Spot checks have been performed against this (more complete) reference: https://arxiv.org/abs/1709.08079

        Currently uses Planck15 cosmology from astropy.
        """

        # Import here in case people don't have/want astropy
        from astropy import units
        from astropy.cosmology import Planck15, z_at_value

        # Horizon distance in Euclidean space
        # FIXME: Horizon needs a snr argument, assumes 8
        dh_euclid = max(self.horizon(snr_threshold, ifos, fmin, fmax, \
                            **kwargs).values())
        z_euclid = \
            z_at_value(Planck15.luminosity_distance, dh_euclid * units.Mpc)

        # User-supplied choice of cosmology calculator. Default is Planck15
        # lal is also minimally supported.
        cosmo = kwargs.pop("cosmology", "Planck15")
        if cosmo == "lal":
            cprms = lal.CosmologicalParameters()
            lal.SetCosmologicalParametersDefaultValue(cprms)
        else:
            cprms = None

        # Readjust source masses until SNR is at correct value again
        def _delta_snr(z, ifo):
            tmplt_args = kwargs.copy()
            if cosmo == "Planck15":
                tmplt_args["distance"] = \
                    Planck15.luminosity_distance(z).to("Mpc").value
            elif cosmo == "lal":
                tmplt_args["distance"] = lal.LuminosityDistance(cprms, z)
            tmplt_args["mass1"] *= 1 + z
            tmplt_args["mass2"] *= 1 + z
            tmplt = eventgen.Event.event_template(**tmplt_args)
            # NOTE: Some care is needed here. If there is more than one ifo in
            # the network, 'optimal' is not appropriate --- there's basically
            # no way to make an event optimal in more than one instrument
            # simultaneously
            return self.snr_at_ifo(tmplt, ifo, optimal=True) - snr_threshold

        # FIXME: Better bounds can be inferred from masses
        z_horizon = {}
        for ifo in ifos or self._instr:
            z_horizon[ifo] = brentq(_delta_snr, \
                                z_euclid * 1e-3, z_euclid * 10, args=(ifo,))
        return z_horizon

    def snr_at_ifo(self, event, ifo, fmin=10., fmax=2048., optimal=False):
        """
        Calculate the matched filtered SNR for this event at a specific instrument. This basically inlines a dictionary lookup.
        """
        return self.snr(event, (ifo,), fmin, fmax, optimal)[ifo]

    def snr(self, event, ifos=None, fmin=10., fmax=2048., optimal=False, **kwargs):
        """
        Calculate the matched filtered SNR for this event over the set of instruments "ifos". If ifos is None, then do so over the entire defined network.
        """
        _remove_wf = False
        if event.hp is None:
            event.gen_waveform(**kwargs)
            _remove_wf = True

        snrs = {}
        for ifo in ifos or self._instr:
            # Make copy to manipulate
            _hp = _to_lal_cmplx_fs(event.hp.data.data)
            det = _detectors[ifo]

            # Cache PSD series for speed --- call _invalidate_cache to clear
            if ifo not in self._psd_cache:
                psd = self._psd_cache[ifo] = _to_lal_real_fs(self._instr[ifo])
            else:
                psd = self._psd_cache[ifo]

            if not optimal:
                gmst = lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(event.event_time))
                fp, fx = lal.ComputeDetAMResponse(det.response, \
                        event.right_ascension, event.declination, \
                        event.polarization, gmst)
            else:
                fp, fx = 1., 0.

            _hp.data.data = fp * event.hp.data.data + fx * event.hx.data.data
            snrs[ifo] = lalsimulation.MeasureSNRFD(_hp, psd, fmin, fmax)

        if _remove_wf:
            event.del_waveform()

        return snrs

    def net_snr(self, event, ifos=None, fmin=10., fmax=2048., optimal=False, **kwargs):
        """
        Calculate the matched filter *network* SNR for all instruments in `ifos`. If this is not provided, use the internal instrument list.
        """
        snrs = numpy.asarray(list(self.snr(event, ifos, fmin, fmax, optimal, **kwargs).values()))
        return math.sqrt(sum(snrs**2))

    def antenna(self, event):
        ant = {}
        for ifo in self._instr:
            resp = lal.cached_detector_by_prefix[ifo]
            gmst = \
                lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(event.event_time))
            fp, fx = lal.ComputeDetAMResponse(resp.response, \
                                              event.right_ascension, \
                                              event.declination, \
                                              event.polarization, gmst)
            ant[ifo] = (fp, fx)

        return ant

    def sum_sq_antenna(self, event, average=False):
        ant = self.antenna(event)
        ant = numpy.square(list(ant.values())).sum()
        if average:
            return numpy.sqrt(ant) / n
        return numpy.sqrt(ant)

if __name__ == "__main__":

    import matplotlib
    matplotlib.use("agg")
    from matplotlib import pyplot
    from scipy.interpolate import interp1d

    aligo = Network(("H1",))

    # net Theta^2 testing
    x = numpy.linspace(0, 1, 100)
    pyplot.plot(x, _fit_theta(x), label="analytic")

    th_sq, ccdf = aligo._theta_sq_net()
    pyplot.plot(x, _fit_theta(x, interp1d(th_sq, ccdf)), label="1 det")

    aligo.add_psd("L1")
    th_sq, ccdf = aligo._theta_sq_net()
    pyplot.plot(x, _fit_theta(x, interp1d(th_sq, ccdf)), label="2 det")

    aligo.add_psd("V1")
    th_sq, ccdf = aligo._theta_sq_net()
    pyplot.plot(x, _fit_theta(x, interp1d(th_sq, ccdf)), label="3 det")

    pyplot.legend()
    #pyplot.show()

    #
    # Usage and unit testing
    #
    network = Network(("H1", "L1", "V1"))

    def snr_threshold_reject(e, nifo=2, threshold=5.5):
        """
        Reject an event that does not have at least 'threshold' SNR in 'nifo' instruments.
        """
        snrs = list(network.snr(e).values())
        if sorted(snrs)[nifo-1] < threshold:
            return False
        return True

    # Have to cut off our sampling in distance somehow
    from functools import partial
    _uniform_comoving = partial(eventgen.uniform_comoving, z_max = 0.5)

    # Generate non-spinning NSBH in comoving volume
    generator = eventgen.EventGenerator()
    generator.append_generator(("mass1", "mass2"), eventgen.uniform_nsbh_comp_mass)
    generator.append_generator(eventgen._spins_cart, eventgen.zero_spin)
    generator.append_generator(("distance",), _uniform_comoving)
    generator.append_generator(eventgen._extr, eventgen.random_orientation)
    generator.append_generator(("eccentricity",), lambda: (0.0,))
    generator.append_generator(("event_time",), lambda: (1e9,))

    # Add post filters
    generator.append_post(("mchirp", "eta"), eventgen.mchirp_eta)

    # Reject anything without a coincidence
    generator.append_conditional(snr_threshold_reject)

    for e in generator.generate_events(n_itr=1):
        print(e.mass1, e.eta, e.distance, e.inclination)
        e.gen_waveform()

        print(network.snr(e))

    #smap = network.bs_localize(e)
    #from lalinference.io import fits
    #fits.write_sky_map("test.fits.gz", smap, nest=True)

    #generator.to_hdf5("test.hdf", "events")
    #generator.to_xml("test.xml.gz")
