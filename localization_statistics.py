
import matplotlib
matplotlib.use("agg")

from argparse import ArgumentParser
from lalinference.io import fits

import skyutils
import numpy as np
import matplotlib.pyplot as plt
import glob
import healpy


# Given an input of names of localized fits files to loop over 
argp = ArgumentParser()
argp.add_argument("-i", "--input-loc", action="append", default=None, help="Path to directory with fits file locations")
argp.add_argument("-o", "--output", default="temp", help="Specify filename for output")
args = argp.parse_args()

# Open file to write sky areas to
output = open(args.output, "w")

# Loop over all fits files
for filename in glob.glob(args.input_loc[0]):
    print('Processing file: %s' % filename)
    sky_data, skymap = skyutils.create_table(filename)
    
    ns = healpy.npix2nside(len(skymap))
    pix_size = healpy.nside2pixarea(ns, degrees=True)
    
    prb50 = np.searchsorted(sky_data["cumul"], 0.50)
    prb68 = np.searchsorted(sky_data["cumul"], 0.68)
    prb90 = np.searchsorted(sky_data["cumul"], 0.90)
    prb95 = np.searchsorted(sky_data["cumul"], 0.95)
    prb99 = np.searchsorted(sky_data["cumul"], 0.99)

    # Extract event number from filename
    event_num = filename.split('/')[-1].split('.')[0]
    print(event_num)
    output.write("%s, %.2f, %.2f \n" % (event_num, prb50 * pix_size, prb90 * pix_size))

output.close()

