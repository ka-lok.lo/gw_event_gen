# kilonovae related formulas
# See the Kilonovae Handbook (Metzger, 2016)
import numpy
from scipy.integrate import simps

import lal
C_CGS = lal.C_SI * 1e2 # convert m to cm
H_CGS = lal.H_SI * 1e7 # convert J to erg
K_CGS = lal.K_SI * 1e7 # convert J to erg
MSUN_CGS = lal.MSUN_SI * 1e3 # convert kg to g
SIGMA_CGS = lal.SIGMA_SI * 1e3 # convert J to erg and m to cm
PC_CGS = lal.PC_SI * 1e2 # convert m to cm

_DEFAULT_M_EJECT = 1e-2 * MSUN_CGS
_DEFAULT_V_MIN = 0.1 * C_CGS
# Note: k_v in SI units is 1e-2 (from 1 cm^2 / g)
_DEFAULT_OPACITY = 1. # g / cm^2

_a = 4 * SIGMA_CGS / C_CGS

_MODULE_VERBOSE=False

def mass_velocity(v, mtot=_DEFAULT_M_EJECT, v_min=_DEFAULT_V_MIN, p_slope=3):
    """
    HB, eqn. 10

    Describes the mass distribution --- as a fraction of the total mass --- having velocity v.
    """
    assert numpy.all(v < C_CGS) # What, you wanna break physics or something!?
    assert numpy.all(v >= v_min)
    return mtot * (v / v_min)**-p_slope

def t_diff(t, mv, mtot=1e-2 * MSUN_CGS, v_min=0.1 * C_CGS, \
                k_v=1., p_slope=3):
    const = mv ** (4./3) * k_v / \
        (4 * numpy.pi * mtot**(1./3) * v_min * C_CGS)
    #v = (mv / mtot)**p_slope * v_min
    #const = 3 * mv * k_v / \
        #(4 * numpy.pi * p_slope * v * C_CGS)
    return const / t

def t_peak(mtot=_DEFAULT_M_EJECT, v=_DEFAULT_V_MIN, k=_DEFAULT_OPACITY):
    """
    HB, eqn. 5

    This is essentially solving for t = t_diff, which is simply a quadratic term.
    """
    return 1.6 * 86400. * numpy.sqrt(mtot / _DEFAULT_M_EJECT / \
            v * _DEFAULT_V_MIN * k / _DEFAULT_OPACITY)

def dE_v_1(v, r_v, e_v):
    """
    p dV energy loss
    """
    if _MODULE_VERBOSE:
        print "\tRadius of outer shell: %e" % r_v[-1]
    return v * e_v / r_v

def dE_v_2(r_v, m_v, t, e_v):
    temp = (3. * e_v / (4 * numpy.pi * _a * r_v**3))**0.25
    if _MODULE_VERBOSE:
        print "\tTemperature of outer shell @ %e: %e" % (r_v[-1], temp[-1])
    k_v = opacity(m_v, r_v, temp)
    if _MODULE_VERBOSE:
        print "\tOpacity of the outer shell: %e" % k_v[-1]
    # FIXME: A lot of the defaults are being used here
    td = t_diff(t, m_v, k_v=k_v)
    if _MODULE_VERBOSE:
        print "\tDiffusion time of the outer shell: %e" % td[-1]
        print "\tLight crossing time of the outer shell: %e" % (r_v[-1] / C_CGS)
    l_v = e_v / (td + r_v / C_CGS)
    if _MODULE_VERBOSE:
        print "\tluminosity of inner shell: %e" % l_v[0]
        print "\tluminosity of outer shell: %e" % l_v[-1]
    return e_v / (td + r_v / C_CGS)

# The x_r is arbitrary, but we assume 1.0 --- all matter is r-process matter
def Q_dot(dM_v, t, x_rv=1.0):
    q = dM_v * x_rv * specific_heating_rate(t)
    if _MODULE_VERBOSE:
        print "\tQ_dot inner shell: %e" % q[0]
        print "\tQ_dot outer shell: %e" % q[-1]
    return q

def therm_efficiency(t):
    """
    HB eqn 23 --- hardcoded values (1e-2 total mass and 0.1 c v_min inserted
    """
    a_v, b_v, d_v = 0.56, 0.17, 0.74
    if t == 0:
        return 0.36 * 2
    return 0.36 * (numpy.exp(-a_v * t) + \
        numpy.log(1 + 2 * b_v * t**d_v) / (2 * b_v * t**d_v))

def specific_heating_rate(t, t0=1.3, sigma=0.11):
    # in erg / s / g ...
    return 4e18 * therm_efficiency(t) * \
        (0.5 - numpy.arctan((t - t0) / sigma) / numpy.pi)**1.3

def bf_opacity(m_v, r_v, t):
    #g_bf, t = ?, ?
    g_bf, _t = 1., 1.
    z, x = 1., 0.
    rho = m_v / (4 / 3. * numpy.pi * r_v**3)
    if _MODULE_VERBOSE:
        print "Density: %e %e" % (rho[0], rho[-1])
    return 4.34e25 * g_bf / _t * z * (1 + x) * rho * t**(-7/2.)

def opacity(m_v, r_v, t):
    # See: https://en.wikipedia.org/wiki/Kramers%27_opacity_law
    # and also: http://www.ucolick.org/~woosley/ay112-14/useful/opacityshu.pdf
    # (eqn. 7.6)
    #if numpy.any(t > 1e4):
        #print "\tSelected bound-free opacity"
        #return bf_opacity(m_v, r_v, t)
    if _MODULE_VERBOSE:
        print "\tSelected fit opacity"
    return _opacity(t)

def _opacity(t):
    k = numpy.zeros(t.shape)
    # FIXME: What do we do outside of these temperatures?
    if False:
        cond1 = (1e3 <= t) & (t < 4e3)
        cond2 = (4e3 <= t) & (t < 1e4)
    else:
        cond1 = t < 4e3
        cond2 = 4e3 <= t
    # FIXME: Last factor of 1000 is for "Lanthanide-free" ejecta (blue)
    k[cond1] = 200. / 100 * (t[cond1] / 4e3)**5.5
    k[cond2] = 200. / 100
    #k[cond1] = 200. * (t[cond1] / 4e3)**5.5
    #k[cond2] = 200.

    if numpy.all(k == 0):
        raise ValueError("No temperatures in range.")
    return k

def eff_temp(e_v, l_v, r_v, m_v):
    l_tot = l_v.sum(axis=0)
    #print "Summed luminosity %e" % l_tot
    temp = (3. * e_v / (4 * numpy.pi * _a * r_v**3))**0.25
    r_ph = photosphere(e_v, r_v, m_v)
    return (l_tot / 4 / numpy.pi / SIGMA_CGS / r_ph**2)**0.25

def optical_depth(m_v, r_v, k_v):
    return m_v * k_v / (4 * numpy.pi * r_v**2)
    #return 3 * m_v * k_v / (4 * numpy.pi * r_v**3)

def photosphere(e_v, r_v, m_v):
    temp = (3. * e_v / (4 * numpy.pi * _a * r_v**3))**0.25
    #print "Temperature of inner shell: %e" % temp[0]
    #print "Temperature of outer shell: %e" % temp[-1]
    try:
        k_v = _opacity(temp)
        # We need to reverse this since the higher radius matter goes
        # transparent first
        dpth = optical_depth(m_v, r_v, k_v) #* (r_v[-1] - r_v)
        idx = numpy.searchsorted(dpth[::-1].cumsum(), 1.)
    except ValueError:
        idx = 0

    # Catch the case where nothing is transparent yet
    if idx == 0:
        return r_v[-1]
    if idx == len(r_v):
        return r_v[0]
    return r_v[::-1][idx]

def bb_flux(freq, t_eff):
    const = 2 * numpy.pi / C_CGS**2 * H_CGS
    return const * freq**3 / \
        (numpy.exp(H_CGS * freq / (K_CGS * t_eff)) - 1)

def generate_light_curve(mtot=1e-2 * MSUN_CGS, t_end=4):

    #
    # Set up mass shells
    #
    # This is the total mass of the ejecta
    #mtot = 1e-2 * MSUN_CGS

    # We set up linearly spaced velocity shells distributed between v_min and some
    # not *too* relativistic velocity.
    v_min = 0.1 * C_CGS
    v_max = 0.3 * C_CGS
    v = numpy.linspace(v_min, v_max, 1001) # fractions of c (m/s)

    # Amount of mass with velocity > v
    m_v = mass_velocity(v, mtot, v_min)
    # Amount of mass in individual shell
    dm = numpy.diff(m_v[::-1])[::-1]

    m_v = m_v[:-1]
    v = v[:-1]

    # Initial energy is dominated by the kinetic energy of the ejecta
    e_init = 0.5 * dm * v**2

    # Initial values
    #r_init = 1e5 # m
    r_init = 1e7 # cm

    # Function we wish to integrate: this is dE/dt
    def fcn(e_v, ti):
        if _MODULE_VERBOSE:
            print "Days since merger: %e" % (ti / 86400.)
        r_v = r_init + v * ti
        q = Q_dot(dm, ti)

        e_v_1 = dE_v_1(v, r_v, e_v)

        try:
            l_v = dE_v_2(r_v, m_v, ti, e_v)
        except ValueError:
            l_v = numpy.zeros(e_v.shape)

        temp = (3. * e_v / (4 * numpy.pi * _a * r_v**3)) ** 0.25

        if _MODULE_VERBOSE:
            print "Temperature of inner and outer shells %e %e" % (temp[0], temp[-1])
            print "pdV loss: %e %e" % (e_v_1[0], e_v_1[-1])
            print "L loss: %e %e" % (l_v[0], l_v[-1])
            print "heating: %e %e" % (q[0], q[-1])
        #import time; time.sleep(1e-5)
        #if numpy.any(l_v > 0):
            #import pdb; pdb.set_trace()
        return -e_v_1 - l_v + q


    from scipy.integrate import odeint
    #t = numpy.linspace(0, 86400 * t_end, 20000)
    t = numpy.linspace(1e-4 * 86400., 86400. * t_end, 20000)
    sol = odeint(fcn, e_init, t)

    ds = 200
    t = t[::ds]
    sol = sol[1::ds]

    r_v = r_init + numpy.outer(t, v)
    temp = 0.25 * numpy.log10((3. * sol / (4 * numpy.pi * _a * r_v**3)))

    # FIXME: Hardcoded frequency bandwidth
    freq = C_CGS / numpy.logspace(-5, -3, 10000)
    flux = numpy.empty((len(t), len(freq)))
    r_ph = []
    for i, e_v in enumerate(sol):
        r_v = r_init + v * t[i]
        r_ph.append(photosphere(e_v, r_v, m_v))
        idx = numpy.searchsorted(r_v, r_ph[-1])

        try:
            l_v = dE_v_2(r_v, m_v, t[i], e_v)
        except ValueError:
            l_v = numpy.zeros(e_v.shape)
        t_eff = eff_temp(e_v, l_v, r_v, m_v)

        flux[i] = bb_flux(freq, t_eff)
        flux[i] *= r_ph[-1]**2

    return t, flux

def bandlimited_mag_at_earth(flux, band="u", dist=200e6 * PC_CGS):
    #
    # Magnitude (pop pop!)
    #

    # FIXME: Hardcoded frequency bandwidth
    freq = C_CGS / numpy.logspace(-5, -3, 10000)

    import photometry

    # h omitted because it cancels in the ratio
    df = numpy.diff(freq[::-1])[::-1]
    df /= freq[::-1][:-1]
    bp = photometry.construct_band(freq, band)
    # Make sure we haven't masked out the entire frequency range
    assert numpy.any(bp)
    df = df[bp[:-1]]
    # the number is Jansky in CGS -- implicit tophat bandpass
    #norm = numpy.sum(df) * 3631e-23
    # NOTE: The integrand has a 1/f in it, which is cancelled by the f from
    # switching the integration measure from df to dln(f)
    # Implicit negative sign is because frequency is decreasing
    norm = 3631e-23 * simps(numpy.ones(freq[bp].shape), numpy.log(freq[bp]))

    # Obtain bandlimited flux
    #bl_flux = numpy.sum(flux[:,bp] * df, axis=1)
    # NOTE: The integrand has a 1/f in it, which is cancelled by the f from
    # switching the integration measure from df to dln(f)
    bl_flux = simps(flux[:,bp], numpy.log(freq[bp]), axis=1)

    # Rescale to distance
    # FIXME: We need the right nomenclature for this, since we've already
    # multiplied by the photospheric radius
    #r_ph = numpy.asarray(r_ph)
    #ab_mag = bl_flux (r_ph / dist)**2
    ab_mag = bl_flux / dist**2

    ab_mag = -2.5 * numpy.log10(ab_mag / norm)

    return freq, ab_mag

#def _generate_janky_light_curve(mtot=1e-2 * MSUN_CGS, dist=200e6 * PC_CGS):
    #t, lc = generate_light_curve(mtot=1e-2 * MSUN_CGS, dist=200e6 * PC_CGS)
def _generate_janky_light_curve(t, lc):
    lc -= 5
    t *= 2
    return t, lc
