# How to Calculate a Matched Filter SNR From a Parameter Set

## From only parameters

If all you have is a set of parameters in mind, then you want to use the `eventgen.Event.event_template` method. As such:

```python
import eventgen

params = {
    "mass1": 35.,
    "mass2": 30.
}

tmplt = eventgen.Event.event_template(**params)
```

Those values not specified in the params correspond to values listed [here](https://git.ligo.org/chris-pankow/gw_event_gen/blob/master/eventgen.py#L352). You can see that you *must* at least specify `mass1` and `mass2`.

## From XML injections

Injections can be read in from an XML file:

```python
import eventgen
import netsim

# Get PSD --- two column format, frequency / PSD value
# Should start at 0 Hz and ascend to Nyquist (usually 2048)
f, psd = numpy.loadtxt("/path/to/psd", unpack=True)

# Set up network
net = netsim.Network()
# This defines an instrument called "H1" with sensitivity (PSD) given by the
# PSD values. If omitted, it will default to the 2G configuration specified for
# the instrument, drawn from lalsimulation.
net.add_psd("H1", psd)

# Pull in injection(s)
xmlfile = "/path/to/xml"
events = eventgen.EventGenerator.from_xml(xmlfile)

for e in events:
    # Not necessary, just an example to see what waveform the code is generating
    e.gen_waveform(flow=20.0, fhigh=1024., fref=21., approximant="SEOBNRv3")
    print e.hp, e.hx

    snrs = net.snr(e, flow=20.0, fhigh=1024., fref=21., approximant="SEOBNRv3")
    # Result is a dictionary ifo / snr pairing
    for ifo, snr in snrs:
        print ifo, snr
```

Alternatively, if working from the `event_template` method, you can go straight to the last few lines:

```python
srs = net.snr(tmplt, ...)
```

Note that if you want a *non-optimal* SNR (e.g. at a given sky location), then a few more parameters should be added to the call:

```python
params = {
    # other params, then...
    "right_ascension": 0.0,
    "declination": 0.0,
    "event_time": 1e9
}
```

If you're interested in horizons, volumes, etc... you might also be interested in `Network.horizon`, `Network.redshift_horizon`, `Network.analytic_volume`. They have similar call signatures.
