import math
import itertools
import numpy
from scipy.spatial.distance import pdist, squareform
import healpy

def ang_dist(p1, p2):
    """
    Determine the angular distance between two points on the unit sphere.
    """
    # FIXME: fp error puts us just over/under +/-1 sometimes
    ip = max(numpy.dot(p1, p2), -1)
    ip = min(ip, +1)
    return numpy.arccos(ip)

def timeline(pos, slew_rate=1., exp_time=40.):
    """
    From a set of pointings, generate a "timeline", e.g. a set of intervals between those pointings.
    """
    xyz_pos = healpy.ang2vec(*pos)
    ang_trav = [ang_dist(*p) for p in zip(xyz_pos[:-1], xyz_pos[1:])]
    ang_trav = numpy.asarray([0] + ang_trav)
    ang_trav /= slew_rate * numpy.pi / 180
    ang_trav += exp_time # static exposure time
    return ang_trav

def get_percentile_mask(smap, prc=0.9):
    # Sort the pixels by probability
    idx = smap.argsort()[::-1]
    cumprob = smap[idx].cumsum()

    # Cut off the map at the proper percentile
    prcntl = numpy.searchsorted(cumprob, prc)

    return idx[:prcntl]

def sky_brute(smap, prc=0.9):
    """
    Brute force strategy, sky map only:
    1. Organize all pixels into sorted order, maximum probability first.
    2. "Observe" each pixel in order, slewing from center to center after each observation.
    3. Tally up the total angular distance travelled.
    """
    idx = get_percentile_mask(smap, prc)

    # Convert pixel order to pointings
    ns = healpy.npix2nside(len(smap))
    pos = healpy.pix2ang(ns, idx)

    return pos

def minmax_prob_time(smap, prc=0.9):
    """
    Accumulate the maximmum amount of probability in the smallest amount of time.
    """
    # Pick a pixel according to its prob. Note that the instrument may be closer
    # to different part of the sky, so this should be accounted for. (Initial
    # location parameter.)

    # Sort the pixels by probability
    idx = get_percentile_mask(smap, prc)
    prob = smap[idx]

    ns = healpy.npix2nside(len(smap))
    ang_pos = numpy.transpose(healpy.pix2vec(ns, idx))
    dist_mat = squareform(pdist(ang_pos, ang_dist))

    # Start at max prob, for now
    all_pos, last_pos = [idx[0]], 0
    for i in range(dist_mat.shape[0]-1):
        # last viewed position
        pos = last_pos
        print "Last viewed index in the d-mat %d / smap %d" % (i, idx[i])
        # mark this as viewed by rolling it back to the current index
        print "Swapping d-mat %d <-> %d" % (pos, i)
        dist_mat[pos], dist_mat[i] = dist_mat[i].copy(), dist_mat[pos].copy()
        dist_mat[:,pos], dist_mat[:,i] = dist_mat[:,i].copy(), dist_mat[:,pos].copy()
        prob[pos], prob[i] = prob[i], prob[pos]
        idx[pos], idx[i] = idx[i], idx[pos]

        # Calculate metric for remaining positions
        # slew size
        dt = dist_mat[i,(i+1):]
        # accumulation of probability versus slew size
        dpdt = prob[(i+1):] / dt
        # maximize
        max_idx = dpdt.argmax()
        dpdt_max = dpdt[max_idx]
        # account for offset from slicing out viewed positions
        max_idx += i + 1
        last_pos = max_idx
        print "Metric maximum is at d-mat idx %d, smap %d with value %e" % (max_idx, idx[max_idx], dpdt_max)
        # record and iterate again
        all_pos.append(idx[max_idx])

    # map back pixel index and position
    return healpy.pix2ang(ns, all_pos)

def bb_optimal_time(smap, prc=0.9):
    """
    Use an initial guess from a heuristic (e.g. nearest neighbor) and perform a version of the branch and bound method to obtain the optimal solution
    """
    # Sort the pixels by probability
    #idx = get_percentile_mask(smap, prc)
    ns = healpy.npix2nside(len(smap))
    smap = smap[:48]
    idx = get_percentile_mask(smap, prc)
    prob = smap[idx]

    ang_pos = numpy.transpose(healpy.pix2vec(ns, idx))
    dist_mat = squareform(pdist(ang_pos, ang_dist))

    # FIXME: We need a dmat argment so as not to recalculate each time
    best_pos = nn_optimal_time(smap, prc)
    bound = timeline(best_pos).sum()
    print "NN bound is %1.2e s" % bound

    rows = set(numpy.arange(len(idx)))
    pos = numpy.asarray(healpy.pix2ang(ns, idx)).T

    # If None gets returned, the bounding solution is optimal
    for comb in itertools.permutations(rows, 2):
        print comb
        rows -= set(comb)
        #pos_trial = [pos[i] for i in comb]
        pos_trial = [dist_mat[comb[0], comb[1]]]
        last = comb[1]

        # FIXME: Check this
        prev_sum = numpy.sum(pos_trial) * 180 / numpy.pi

        new_bound, new_pos = _append_and_check(rows, dist_mat, last, pos_trial, bound=bound, prev_sum=prev_sum)
        if new_bound < bound:
            assert new_pos is not None
            bound, best_pos = new_bound, [comb[0]] + new_pos
        rows |= set(comb)

    # return optima
    return healpy.pix2ang(ns, best_pos)

def _append_and_check(rows, dist_mat, last, pos_trial, bound=float("inf"), prev_sum=0):
    new_pos = None
    #trial = timeline(numpy.transpose(pos_trial))
    # If this particular chain is already past our bound, bail
    #print "Checking trial of len %d" % len(pos_trial)
    #bnd = numpy.sum(pos_trial) * 180 / numpy.pi + 40 * len(pos_trial)
    prev_sum += last * 180 / numpy.pi + 40
    if prev_sum > bound:
        #print "Trial of len %d rejected %1.4e > %1.4e" % (len(pos_trial), prev_sum, bound)
        return bound, None
    elif len(pos_trial) == len(dist_mat) - 1:
        print "Trial of len %d candidate %1.4e < %1.4e" % (len(pos_trial), prev_sum, bound)
        return prev_sum, [last]

    # Iterate though the next "link"
    for r in rows:
        rows.remove(r)
        new_bound, tmp = _append_and_check(rows, dist_mat, r, \
                pos_trial + [dist_mat[last,r]], bound=bound, prev_sum=prev_sum)
        if new_bound < bound:
            assert tmp is not None
            bound, new_pos = new_bound, [last] + tmp
        rows.add(r)

    return bound, new_pos

def nn_optimal_time(smap, prc=0.9):
    """
    Use nearest-neighbor heuristic to find an approximate best solution.
    """
    # Sort the pixels by probability
    idx = get_percentile_mask(smap, prc)
    prob = smap[idx]

    ns = healpy.npix2nside(len(smap))
    ang_pos = numpy.transpose(healpy.pix2vec(ns, idx))
    dist_mat = squareform(pdist(ang_pos, ang_dist))

    # Start at max prob, for now
    all_pos, last_pos = [idx[0]], 0
    for i in range(dist_mat.shape[0]-1):
        # last viewed position
        pos = last_pos
        print "Last viewed index in the d-mat %d / smap %d" % (i, idx[i])
        # mark this as viewed by rolling it back to the current index
        print "Swapping d-mat %d <-> %d" % (pos, i)
        dist_mat[pos], dist_mat[i] = dist_mat[i].copy(), dist_mat[pos].copy()
        dist_mat[:,pos], dist_mat[:,i] = dist_mat[:,i].copy(), dist_mat[:,pos].copy()
        prob[pos], prob[i] = prob[i], prob[pos]
        idx[pos], idx[i] = idx[i], idx[pos]

        # Calculate metric for remaining positions
        # slew size
        dt = dist_mat[i,(i+1):]
        # maximize
        min_idx = dt.argmin()
        dt_min = dt[min_idx]
        # account for offset from slicing out viewed positions
        min_idx += i + 1
        last_pos = min_idx
        print "Metric maximum is at d-mat idx %d, smap %d with value %e" % (min_idx, idx[min_idx], dt_min)
        # record and iterate again
        all_pos.append(idx[min_idx])

    # map back pixel index and position
    return healpy.pix2ang(ns, all_pos)

def resample_sky_map(smap, new_res=3.5**2, verbose=False):
    """
    Resample a healpix skymap from its native resolution to a new specified resolition. Note that this is a discretized process, that is the resolution will be as close as possible to that which was requested, but due to the limitations of healpix, cannot be exact.
    In reality, this calculates the `nside` parameter which most closely matches the desired angular resolution, and uses `healpy.ud_grade` to resample the map.
    """
    ns = healpy.npix2nside(len(smap))
    res = healpy.nside2pixarea(ns, degrees=True)
    if verbose:
        print "Native resolution: %e sq. deg." % res

    # new FoV vs native resolution
    ds = new_res / res
    ns_ds = ns / math.sqrt(ds)
    ns_ds = 2**int(math.ceil(math.log(ns_ds, 2)))
    if verbose:
        print "Downsampling factor (approx.) %1.3f" % ds
        print "New pixel area: %.3f" % healpy.nside2pixarea(ns_ds, degrees=True)

    # resample skymap to be closer to instrument FoV
    return healpy.pixelfunc.ud_grade(smap, ns_ds, power=-2)


if __name__ == "__main__":

    # Generate random healpix compatible sky map
    smap = numpy.random.uniform(0, 1, 12 * 128**2)
    smap /= smap.sum()

    # Resample down to a different resolution
    smap_ds = resample_sky_map(smap, verbose=True)
    ns = healpy.npix2nside(len(smap_ds))

    # Brute force, start at highest prob and work downwards
    pos = sky_brute(smap_ds)

    # Sum over the angular distance traversed
    xyz_pos = healpy.ang2vec(*pos)
    ang_trav = numpy.asarray([ang_dist(*p) for p in \
                    zip(xyz_pos[:-1], xyz_pos[1:])])
    print ang_trav.sum()

    smap_graph = construct_sm_graph(smap_ds)
    for pix_node in smap_graph:
        [d for l, d in pix_node]
