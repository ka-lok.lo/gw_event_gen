#!/bin/bash

# generate psd
./generate_psd

# generate sim
./generate_events --generate-bns -N 1 --verbose --write-output test_bns_1.xml.gz

bayestar_realize_coincs --detector H1 L1 --reference-psd psd.xml.gz --output sim_bstar_coinc.xml.gz test_bns_1.xml.gz --f-low 20 --waveform IMRPhenomPv2 --net-snr-threshold 0.0

bayestar_localize_coincs --keep-going --f-low 20 --waveform TaylorF2threePointFivePN sim_bstar_coinc.xml.gz -l INFO
