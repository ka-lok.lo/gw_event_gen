"""
Bandpass parameters
"""
import numpy

import lal

# https://en.wikipedia.org/wiki/Photometric_system
# FIXME: These are actually the uppercase filters, need to get proper descriptions of the LSST bands
_bands = {
    "u": {
        "center": 385e-9,
        "fwhm": 94e-9
    },
    "b": {
        "center": 445e-9,
        "fwhm": 94e-9
    },
    # V = G?
    "g": {
        "center": 551e-9,
        "fwhm": 88e-9
    },
    "r": {
        "center": 658e-9,
        "fwhm": 138e-9
    },
    "i": {
        "center": 806e-9,
        "fwhm": 144e-9
    },
    "z": {
        "center": 900e-9,
        # FIXME: ad-hoc, not given on wiki page
        "fwhm": 100e-9
    },
    "y": {
        "center": 1020e-9,
        # FIXME: ad-hoc, not given on wiki page
        "fwhm": 120e-9
    },
}

lsst_bands = set(("u", "g", "r", "i", "z", "y"))

def sort_by_band(bands):
    """
    Produce an ordering of bandpasses by their central wavelength.
    """
    order = ["u", "g", "r", "i", "z", "y"]
    sbands = [None] * len(bands)
    i = len(sbands) -1
    for b in bands:
        _b = b[0]
        try:
            sbands[order.index(_b)] = b
        except ValueError:
            sbands[i] = b
            i -= 1

    return sbands

def construct_band(freq, band):
    """
    Construct a bandpass filter from the frequencies given in freq, and a specified bandpass (band).
    """
    bp = _bands[band]
    l = lal.C_SI / (bp["center"] + bp["fwhm"] / 2)
    u = lal.C_SI / (bp["center"] - bp["fwhm"] / 2)
    return (freq > l) & (freq < u)

def get_spectrum(bands=None, nbins=10000):
    """
    Generate a set of log-spaced frequencies spanning the full set of bandpass filters available in the module.
    """
    l, u = float("inf"), float("-inf")
    if bands is None:
        for bp in _bands.itervalues():
            bp_l = bp["center"] - bp["fwhm"] / 2
            bp_u = bp["center"] + bp["fwhm"] / 2
            l = min(l, bp_l)
            u = max(u, bp_u)
    return lal.C_SI / numpy.logspace(numpy.log10(l), numpy.log10(u), nbins)

if __name__ == "__main__":
    print get_spectrum()
    print lal.C_SI / numpy.logspace(-7, -5, 10000)
    # Make a 'rainbow' plot of the filters
